'use strict';

var mongoose = require('mongoose'),
  async = require('async'),
  commandLineArgs = require('command-line-args'),
  redis = require('redis'),
  axios = require('axios'),
  cmd = require('node-cmd'),
  mg = require('../config/lib/mongoose');

var cache;
var lastCallMoment = 0;
var _minInterval = 1;
var _maxInterval = 2;


async.waterfall([
  // connect to db
  function (done) {
    mg.loadModels();

    mg.connect(function (db) {
      console.log('connect to mongoDB');
      done(null, db);
    });
  },
  function (db, done) {
    doCommand();
  }
]);

function timestamp() {
  return new Date(); //Math.floor(new Date().getTime() / 1000);
}

var _log;
function logT(...argv) {
  argv = [new Date()].concat(argv);
  _log.apply(null, argv);
}
_log = console.log;
console.log = logT;

function makeRequest() {
  return new Promise(function (resolve, reject) {
    var current = new Date().getTime() / 1000;
    if (current - lastCallMoment > _minInterval) {
      lastCallMoment = current;
      resolve();
    } else {
      setTimeout(() => {
        current = new Date().getTime() / 1000;
        lastCallMoment = current;
        resolve();
      }, (Math.random() * _maxInterval + _minInterval) * 1000);
    }
  });
}

function doCommand() {

  connectRedis();

  const mainOptions = commandLineArgs([{ name: 'command', defaultOption: true }], { stopAtFirstUnknown: true });
  const argv = mainOptions._unknown || [];

  // if (mainOptions.command === 'genre' || mainOptions.command === 'g') {
  
  doScarapCollection(argv);
}

function beginCommand(work) {
  cache.set('current_work', work);
  cache.set('current_work_status', 'processing');
}

function endCommand(status) {
  cache.set('current_work_status', status);
  process.exit();
}

function _buildExistList() {
  var existMap = {}
  return new Promise(function (resolve, reject) {
    async.waterfall([
      function (done) {
        var MrManga = mongoose.model('MrManga');
        MrManga.find({}, function(err, mangas) {
          existMap.mangas = {};
          (mangas || []).forEach(function(a) {
            existMap.mangas[a.oid] = a;
          });
          console.log('build exist manga map, found ' + mangas.length);
          done();
          });
      },
      function (done) {
        var MrAuthor = mongoose.model('MrAuthor');        
        MrAuthor.find({}, function(err, authors) {
          existMap.authors = {};
          (authors || []).forEach(function(a) {
            existMap.authors[a.oid] = a;
          });    
          console.log('build exist author map, found ' + authors.length);
          done();
        });
      },
      function (done) {
        var MrGenre = mongoose.model('MrGenre');
        MrGenre.find({}, function(err, genres) {
          existMap.genres = {};
          (genres || []).forEach(function(a) {
            existMap.genres[a.oid] = a;
          });
          console.log('build exist genre map, found ' + genres.length);
          done();
        });
      },
      function (done) {
        resolve(existMap);
        done();
      }
    ]);
  });
}

function connectRedis() {
  cache = redis.createClient();
}

function doScarapCollection() {
  beginCommand('collection');
  makeRequest().then(function() {
    console.log('read collection for you');
    axios.get('https://api.mangarockhd.com/query/web400/foryou?msid=71&country=United%20States')
    .then(function (response) {
      //  console.log(response);
      if (response.status == 200 && response.data.code == 0) {
        var rawData = response.data.data || [];
        readTrack(rawData, onFinishedWork)
      } else {
        console.log('error on http request' + response.status);
        endCommand('error on http request' + response.status);
      }
    })
    .catch(function (error) {
      console.error('error on reading collection for you', error);
      endCommand('failed to request service');
    });
  });

  function onFinishedWork(err, result) {
    console.log('done work');
    endCommand('complete');
  }
}

function readTrack(rawTrack, callback) {
  async.waterfall([
    function (done) {
      // console.log('checking current track');
      // var MrTracking = mongoose.model('MrTracking');
      // MrTracking.findOne({oid: rawTrack.tracking_id}, function(err, track) {
      //   if (err) {
      //     return done(err);
      //   }
      //   if (track) {
      //     console.log('already exist ' + rawTrack.tracking_id);
      //     done('already exists collect of ' + rawTrack.tracking_id);
      //   } else {
          done(null, rawTrack);
      //   }
      // });
    },
    function (rawTrack, done) {
      console.log(rawTrack);

      var MrTracking = mongoose.model('MrTracking');
      var MrSection = mongoose.model('MrSection');
      var MrCollection = mongoose.model('MrCollection');

      var track = {
        oid: rawTrack.tracking_id,
        sections: []
      };

      readSection(rawTrack.sections, function (err, sections) {
        if (err) {
          console.log(err);              
          return done(err);
        } else {
          track.sections = sections;
          MrTracking.findOneAndUpdate({
            oid: track.oid
          }, track, { new: true, upsert: true }, function (err, track) {
            if (err) {
              console.log(err);
              return done(err);
            } else {
              return done();
            }
          });

        }
      });

      function readSection(rawSections, callback) {
        function _readSection(rawSections, result, callback) {
          var rawSection = rawSections.shift();
      
          if (!rawSection) {
            return callback(null, result);
          }

          var rawCollections = rawSection.items.data;
          readCollection(rawCollections, function(err, collections) {
            if (err) {
              return callback(err);
            }
            var section = {
              oid: rawSection.sectionId,
              title: rawSection.title.en ? rawSection.title.en : '',
              subtitle: rawSection.subtitle.en ? rawSection.subtitle.en : '',
              type: rawSection.type,
              items: {
                component: rawSection.items.component,
                data: collections
              }
            };

            MrSection.findOneAndUpdate({
              oid: rawSection.sectionId
            }, section, { new: true, upsert: true }, function (err, section) {
              if (err) {
                console.log('failed on saving section of ' + rawSection.sectionId);
                return callback(err, result);
              } else {
                console.log('saved section of ' + rawSection.sectionId);
                result.push(section);
                _readSection(rawSections, result, callback);
              }
            });
          });
        }
        _readSection(rawSections, [], function(err, result) {
          callback(err, result);
        })        
      }

      function readCollection(rawCollections, callback) {
        function _readCollection(rawCollections, result, callback) {
          var rawCollection = rawCollections.shift();
      
          if (!rawCollection) {
            return callback(null, result);
          }

          axios.get('https://api.mangarockhd.com/query/web400/collection?country=United%20States&oid=' + rawCollection.oid)
          .then(function (response) {
            //  console.log(response);
            if (response.status == 200 && response.data.code == 0) {
              var rawData = response.data.data || [];
              
              var collection = {
                oid: rawData.oid,
                name: rawData.name,
                description: rawData.description,
                banner: rawData.banner,
                header_type: rawData.header_type,
                header_data: rawData.header_data,
                item_layout: rawData.item_layout,
                item_type: rawData.item_type,
                items: rawData.items
              };
              
              MrCollection.findOneAndUpdate({
                oid: rawData.oid
              }, collection, { new: true, upsert: true }, function (err, collection) {
                if (err) {
                  console.log('failed on saving collection of ' + rawData.oid);
                  return callback(err, result);
                }
    
                result.push(collection);
                console.log('saved collection of ' + rawData.oid);
                _readCollection(rawCollections, result, callback);
              });
            } else {
              console.log('error on http request' + response.status);
              endCommand('error on http request' + response.status);
            }
          })
          .catch(function (error) {
            console.error('error on reading collection for you', error);
            endCommand('failed to request service');
          })
        }
        _readCollection(rawCollections, [], function(err, result) {
          callback(err, result);
        })
      }
    },
    function (done) {
      endCommand('complete');
    }
  ], function (err, result) {
    console.log(err, result);
    endCommand('error');
  });
}
