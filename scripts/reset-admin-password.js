'use strict';

var mongoose = require('mongoose'),
  chalk = require('chalk'),
  config = require('../config/config'),
  mg = require('../config/lib/mongoose');

mg.loadModels();

mg.connect(function (db) {
  var retry_count = 0;
  var stdin = process.openStdin();
  var User = mongoose.model('User');

  User.findOne({'username': 'otakumaster'}).exec(function (err, user) {
    if (err) {
      console.log('Upexpected error happends, retry again later');
      process.exit(0);  
    } else if (!user) {
      console.log('Not found admin user, contact to system manager or mail to covernal@hotmail.com');
      process.exit(0);  
    } else {
      console.log("New password:");
      stdin.addListener("data", function (password) {
        password = password.toString();
        if (password.length <= 4) {
          if (++retry_count < 3) {
            console.log("Bad password, new password:");
            return;
          } else {
            console.log("Nothing updated");
            process.exit(0);
          }
        }
        password = password.substring(0, password.length - 1);
        user.password = password;
        console.log(user);
        user.save(function (err) {
          if (err) {
            console.log('Upexpected error happends, retry again later');
          }
          process.exit(0);
        });
      });    
    }
  });
});
