'use strict';

var mongoose = require('mongoose'),
  async = require('async'),
  commandLineArgs = require('command-line-args'),
  redis = require('redis'),
  axios = require('axios'),
  cmd = require('node-cmd'),
  mg = require('../config/lib/mongoose');

var cache;
var lastCallMoment = 0;
var _minInterval = 1;
var _maxInterval = 2;


async.waterfall([
  // connect to db
  function (done) {
    mg.loadModels();

    mg.connect(function (db) {
      console.log('connect to mongoDB');
      done(null, db);
    });
  },
  function (db, done) {
    doCommand();
  }
]);

function timestamp() {
  return new Date(); //Math.floor(new Date().getTime() / 1000);
}

var _log;
function logT(...argv) {
  argv = [new Date()].concat(argv);
  _log.apply(null, argv);
}
_log = console.log;
console.log = logT;

function makeRequest() {
  return new Promise(function (resolve, reject) {
    var current = new Date().getTime() / 1000;
    if (current - lastCallMoment > _minInterval) {
      lastCallMoment = current;
      resolve();
    } else {
      setTimeout(() => {
        current = new Date().getTime() / 1000;
        lastCallMoment = current;
        resolve();
      }, (Math.random() * _maxInterval + _minInterval) * 1000);
    }
  });
}

function doCommand() {

  connectRedis();

  const mainOptions = commandLineArgs([{ name: 'command', defaultOption: true }], { stopAtFirstUnknown: true });
  const argv = mainOptions._unknown || [];

  if (mainOptions.command === 'genre' || mainOptions.command === 'g') {
      doScarapGenres(argv);
  } else if (mainOptions.command === 'manga' || mainOptions.command === 'm') {
      doScrapManga(argv);
  }
}

function beginCommand(work) {
  cache.set('current_work', work);
  cache.set('current_work_status', 'processing');
}

function endCommand(status) {
  cache.set('current_work_status', status);
  process.exit();
}

function _buildExistList() {
  var existMap = {}
  return new Promise(function (resolve, reject) {
    async.waterfall([
      function (done) {
        var MrManga = mongoose.model('MrManga');
        MrManga.find({}, function(err, mangas) {
          existMap.mangas = {};
          (mangas || []).forEach(function(a) {
            existMap.mangas[a.oid] = a;
          });
          console.log('build exist manga map, found ' + mangas.length);
          done();
          });
      },
      function (done) {
        var MrAuthor = mongoose.model('MrAuthor');        
        MrAuthor.find({}, function(err, authors) {
          existMap.authors = {};
          (authors || []).forEach(function(a) {
            existMap.authors[a.oid] = a;
          });    
          console.log('build exist author map, found ' + authors.length);
          done();
        });
      },
      function (done) {
        var MrGenre = mongoose.model('MrGenre');
        MrGenre.find({}, function(err, genres) {
          existMap.genres = {};
          (genres || []).forEach(function(a) {
            existMap.genres[a.oid] = a;
          });
          console.log('build exist genre map, found ' + genres.length);
          done();
        });
      },
      function (done) {
        resolve(existMap);
        done();
      }
    ]);
  });
}

function doScrapManga(argv) {
  async.waterfall([
    function (done) {
      beginCommand('manga');
      console.log('build exist map for mangas and authors');
      _buildExistList().then(function (existMap) {
        done(null, existMap);
      })
    },
    function (existMap, done) {      
      makeRequest().then(function() {    
        console.log('read indexes of all mangas');
        axios.post('https://api.mangarockhd.com/query/web400/mrs_filter?country=United%20States', {
          "status":"all",
          "genres":{},
          "rank":"all",
          "order":"rank"
        }).then(function (response) {
          // console.log(response);
          if (response.status == 200 && response.data.code == 0) {
            var mangaIdxs = response.data.data || [];
            console.log(mangaIdxs.length + 's of manga are in the list');
            done(null, existMap, mangaIdxs);
          } else {
            console.log('failed to read managas', response);            
            endCommand('error');            
          }
        })
        .catch(function (error) {
          console.log(error);
          endCommand('error');
        });
      });
    },
    function (existMap, mangaIdxs, done) {
      readMangas(existMap, mangaIdxs, function(err, result) {
        if (err) return done(err, result);
        done();
      });
    },
    function (done) {
      done();
      endCommand('complete');
    }
  ], function (err, result) {
    console.log(err, result);
    endCommand('error');
  });
}

function readMangas(existMap, mangaIdxs, callback) {
  var mangaID = mangaIdxs.shift();

  if (mangaID === '') {
    return setTimeout(() => {
      console.log('skip EMPTY mangaID, exist already on the db, has left ' + mangaIdxs.length);
      readMangas(existMap, mangaIdxs, callback);
    }, 0);
  }
  else if (!mangaID) {
    return callback(null, 'complete');
  }

  if (existMap.mangas[mangaID]) {
    if (existMap.mangas[mangaID].completed) {
      console.log('skip ' + mangaID + ', exist already on the db, has left ' + mangaIdxs.length);
      return setTimeout(() => {
        readMangas(existMap, mangaIdxs, callback);
      }, 0);
    } else {

    }
  };

  makeRequest().then(function() {
    console.log('read ' + mangaID + ', has left ' + mangaIdxs.length);
    axios.get('https://api.mangarockhd.com/query/web400/info?last=0&country=United%20States&oid=' + mangaID)
    .then(function (response) {
      // console.log(response);
      if (response.status == 200 && response.data.code == 0) {
        var rawManga = response.data.data || {};
        writeManga(existMap, rawManga, function(err, result) {          
          if (err) {
            console.log('error on writing for ' + mangaID, err)
            return readMangas(existMap, mangaIdxs, callback);
          } else if (result) {
            console.log('imported manga of ' + result.oid + ' as fully, has left ' + mangaIdxs.length);
            existMap.mangas[result.oid] = result;
            return readMangas(existMap, mangaIdxs, callback);
          }
        })
      } else {
        return readMangas(existMap, mangaIdxs, callback);
        console.log('failed reading of ' + result.oid + ' as fully');
      }
    })
    .catch(function (error) {
      callback('error', error);
    });
  });
}

function writeManga(existMap, rawManga, callback) {
  var authorIdxs = [];
  var genreIdxs = [];
  var charactersIdxs = [];

  var chapterIdxs = [];
  var chapterRaws = [];  
  var mapChapter = {};

  (rawManga.authors || []).forEach(function (a) {
    authorIdxs.push(a.oid);
  });

  (rawManga.characters || []).forEach(function (a) {
    charactersIdxs.push(a.oid);
  });

  (rawManga.rich_categories || []).forEach(function (a) {
    genreIdxs.push(a);
  });

  (rawManga.chapters || []).forEach(function (a) {
    a.manga_oid = rawManga.oid;
    chapterRaws.push(a);
    chapterIdxs.push(a.oid)
  });

  async.waterfall([
    function (done) {
      console.log('read authors for manga of ' + rawManga.oid);
      readAuthor(existMap.authors, authorIdxs.concat(charactersIdxs), function(err, result) {
        if (err) {
          done(err);
        } else {
          console.log('done reading authors & characters for manga of ' + rawManga.oid);
          // console.log(existMap.authors.length);
          done();
        }
      });
    },
    function (done) {
      var MrChapter = mongoose.model('MrChapter');
      MrChapter.find({
        'oid': { $in : chapterIdxs}
      }, function (err, docs) {
        (docs || []).forEach(function (a) {
          mapChapter[a.oid] = a;
        })
        done();
      });
    },
    function (done) {
      console.log('read chapters for manga of ' + rawManga.oid);
      readChapter(mapChapter, chapterRaws, function(err, result) {
        if (err) {
          done(err);
        } else {
          console.log('done reading chapters for manga of ' + rawManga.oid);
          mapChapter = result;
          done();
        }
      });
    },
    function (done) {
      rawManga.lowerName = rawManga.name.toLowerCase();
      rawManga.categories = [];
      genreIdxs.forEach(function(a) {
        if (existMap.genres[a.oid]) rawManga.categories.push(existMap.genres[a.oid]);
      });
      rawManga.chapters = [];
      chapterIdxs.forEach(function(a) {
        if (mapChapter[a]) rawManga.chapters.push(mapChapter[a]);
        // console.log('add chapter ' + a.oid);
      });
      rawManga.authors = [];
      authorIdxs.forEach(function(a) {
        // console.log(a, existMap.authors);
        if (existMap.authors[a]) rawManga.authors.push(existMap.authors[a]);
      });
      rawManga.characters = [];
      charactersIdxs.forEach(function(a) {
        if (existMap.authors[a]) rawManga.characters.push(existMap.authors[a]);
      });
      
      var MrManga = mongoose.model('MrManga');
      MrManga.seed(rawManga, { overwrite: true })
      .then(function (response,) {
        var _manga = response.added;
        callback(null, _manga);
      })
      .catch(function (error) {
        callback(error);
      });   
    }
  ], function (err) {
    callback(err);
  });
}

function doScrapAuthor() {
  beginCommand('author');

  makeRequest().then(function() {
    console.log('read indexes of all authors');
    axios.post('https://api.mangarockhd.com/query/web400/mrs_all_author?country=United%20States', {
      "name":"",
    }).then(function (response) {
      // console.log(response);
      if (response.status == 200 && response.data.code == 0) {
        var authorIdxs = response.data.data || [];
        console.log(authorIdxs.length + 's of author are in the list');
        _updateAuthorDB(authorIdxs)    
      } else {
        endCommand('error');
      }
    })
    .catch(function (error) {
      console.log(error);
      endCommand('error');
    });
  });

  function _updateAuthorDB(authorIdxs) {
    var MrAuthor = mongoose.model('MrAuthor');        
    MrAuthor.find({}, function(err, authors) {
      var existMap = {};
      (authors || []).forEach(function(a) {
        existMap[a.oid] = a;
      });    
      readAuthor(existMap, authorIdxs, function(err, result) {
        if (err) {
          return endCommand('error');
        } else {
          console.log('completed update author database');
          return endCommand('complete');
        }
      });
    });
  }
}

function readAuthor(existMap, authorIdxs, callback) {
  var authorID = authorIdxs.shift();

  if (!authorID) {
    return callback(null, 'complete');
  }
  if (existMap[authorID]) {
    console.log('skip ' + authorID + ', exist already on the db');
    return setTimeout(() => {
      readAuthor(existMap, authorIdxs, callback);
    }, 0);
  };

  var MrAuthor = mongoose.model('MrAuthor');
  makeRequest().then(function() {
    console.log('read ' + authorID + ', has left ' + authorIdxs.length);
    var url = '';
    if (authorID.indexOf('mrs-author') >= 0) {
      url = 'https://api.mangarockhd.com/query/web400/author?oid=' + authorID;
    } else {      
      url = 'https://api.mangarockhd.com/query/web400/character?oid=' + authorID;
    }
    axios.get(url).then(function (response) {
      // console.log(response);
      if (response.status == 200 && response.data.code == 0) {
        var rawAuthor = response.data.data || {};
        rawAuthor.lowerName = rawAuthor.name.toLowerCase();
        MrAuthor.seed(rawAuthor, { overwrite: true })
        .then(function (response){
          var _author = response.added;
          existMap[_author.oid] = _author;
          readAuthor(existMap, authorIdxs, callback);
        })
        .catch(function (error) {
          readAuthor(existMap, authorIdxs, callback);
        });          
      } else {
        readAuthor(existMap, authorIdxs, callback);
      }
    })
    .catch(function (error) {
      console.log(error);
      callback(error);
    });
  });
}

function readChapter(existMap, chapterRaws, callback) {
  var rawChapter = chapterRaws.shift();

  if (!rawChapter) {
    return callback(null, existMap);
  }

  if (existMap[rawChapter.oid]) {
    console.log('skip ' + rawChapter.oid + ', exist already');
    return setTimeout(() => {
      readChapter(existMap, chapterRaws, callback);
    }, 0);    
  };

  var MrChapter = mongoose.model('MrChapter');
  makeRequest().then(function() {
    console.log('read ' + rawChapter.oid + ', has left ' + chapterRaws.length);
    axios.get('https://api.mangarockhd.com/query/web400/pages?country=United%20States&oid=' + rawChapter.oid)
    .then(function (response) {
      // console.log(response);
      if (response.status == 200 && response.data.code == 0) {
        var rawPages = response.data.data || [];        
        rawChapter.pages = rawPages;
        MrChapter.seed(rawChapter, { overwrite: true })
        .then(function (response) {
          var _chapter = response.added;
          existMap[_chapter.oid] = _chapter;
          readChapter(existMap, chapterRaws, callback);
        })
        .catch(function (error) {
          readChapter(existMap, chapterRaws, callback);
        });          
      } else {
        readChapter(existMap, chapterRaws, callback);
      }
    })
    .catch(function (error) {
      console.error('error on reading '  + rawChapter.oid);
      chapterRaws.unshift(rawChapter);
      readChapter(existMap, chapterRaws, callback);
      // callback(error);
    });
  });
}

function connectRedis() {
  cache = redis.createClient();
}


function doScarapGenres() {
  async.waterfall([
    function (done) {
      beginCommand('manga');
      console.log('build exist genre list');
      var MrGenre = mongoose.model('MrGenre');
      MrGenre.find({}, function(err, genres) {
        if (err) {
          return done(err);
        }
        console.log('build exist genre list, found ' + genres.length);
        done(null, genres);
      });

    },
    function (genres, done) {
      function readGenres(genres, callback) {
        var genre = genres.shift();
      
        if (!genre) {
          return callback(null);
        }
      
        var MrChapter = mongoose.model('MrChapter');
        makeRequest().then(function() {
          console.log('read ' + genre.oid + ', has left ' + genres.length);
          axios.get('https://api.mangarockhd.com/query/web400/genre?country=United%20States&oid=' + genre.oid)
          .then(function (response) {
            // console.log(response);
            if (response.status == 200 && response.data.code == 0) {
              var rawData = response.data.data || [];        
              genre.cover = rawData.cover;
              genre.description = rawData.description;
              genre.save(function (err) {
                readGenres(genres, callback);
              });
            } else {
              readGenres(genres, callback);
            }
          })
          .catch(function (error) {
            console.error('error on reading '  + genre.oid);
            genres.unshift(genre);
            readGenres(genres, callback);
            // callback(error);
          });
        });
      }

      readGenres(genres, function (err) {
        if (err) {
          return done(err);
        } else {
          done ();
        }
      })
    },
    function (done) {
      endCommand('complete');
    }
  ], function (err, result) {
    console.log(err, result);
    endCommand('error');
  });
}