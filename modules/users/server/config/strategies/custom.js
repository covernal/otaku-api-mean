'use strict';

/**
 * Module dependencies
 */
var passport = require('passport'),
CustomStrategy = require('passport-custom').Strategy,
  User = require('mongoose').model('User');

module.exports = function () {
  // Use local strategy
  passport.use(new CustomStrategy(function (req, done) {
    console.log(req.body);
    User.findOne({
      $or: [
        // {
        //   username: usernameOrEmail.toLowerCase()
        // },
        {
          email: (req.body.email || '').toLowerCase()
        }
      ]
    }, function (err, user) {
      if (err) {
        return done(err);
      }
      if (!user) {
        delete req.body.roles;

        // Init user and add missing fields
        var user = new User(req.body);
        user.provider = 'local';
        user.displayName = user.firstName + ' ' + user.lastName;
        user.password = 'otakulife.app';
      
        // Then save the user
        user.save(function (err) {
          if (err) {
            return done(err);
          } else {
            // Remove sensitive data before login
            user.password = undefined;
            user.salt = undefined;
      
            return done(null, user);
          }
        });
        // return done(null, false, {
        //   message: 'Invalid username (' + (new Date()).toLocaleTimeString() + ')'
        // });
      }
      else {
        return done(null, user);
      }
    });
  }));
};
