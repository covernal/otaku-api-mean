(function (app) {
  'use strict';

  app.registerModule('mangarock');
  app.registerModule('mangarock.routes', ['ui.router', 'core.routes']);
  app.registerModule('mangarock.services');
  app.registerModule('mangarock.admin.services');
}(ApplicationConfiguration));
