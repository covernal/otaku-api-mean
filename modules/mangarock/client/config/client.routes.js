(function () {
  'use strict';

  // Setting up route
  angular
    .module('mangarock.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    // Users state routing
    $stateProvider
      .state('mangarock', {
        abstract: true,
        url: '/home',
        templateUrl: '/modules/mangarock/client/views/home.view.html',
        controller: 'MangaHomeController',
        controllerAs: 'vm',
        data: {
          roles: ['user', 'admin']
        }
      })
      .state('mangarock.dash', {
        url: '/dash',
        templateUrl: '/modules/mangarock/client/views/dash.view.html',
        // controller: '',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Settings'
        }
      })
      .state('mangarock.ads', {
        url: '/ads',
        templateUrl: '/modules/mangarock/client/views/ads.view.html',
        // controller: '',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Settings'
        }
      })
      .state('mangarock.users', {
        url: '/users',
        templateUrl: '/modules/mangarock/client/views/list-users.client.view.html',
        controller: 'AdminUserListController',
        controllerAs: 'vm'
      })
      .state('mangarock.user', {
        url: '/users/:userId',
        templateUrl: '/modules/mangarock/client/views/view-user.client.view.html',
        controller: 'AdminUserController',
        controllerAs: 'vm',
        resolve: {
          userResolve: getUser
        },
        data: {
          pageTitle: '{{ userResolve.displayName }}'
        }
      })
      .state('mangarock.user-edit', {
        url: '/users/:userId/edit',
        templateUrl: '/modules/mangarock/client/views/edit-user.client.view.html',
        controller: 'AdminUserController',
        controllerAs: 'vm',
        resolve: {
          userResolve: getUser
        },
        data: {
          pageTitle: '{{ userResolve.displayName }}'
        }
      })
      .state('mangarock.import', {
        url: '/import',
        templateUrl: '/modules/mangarock/client/views/import.view.html',
        controller: 'MangaImportController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Settings'
        }
      });


    getUser.$inject = ['$stateParams', 'AdminService'];

    function getUser($stateParams, AdminService) {
      return AdminService.get({
        userId: $stateParams.userId
      }).$promise;
    }
  }
}());
