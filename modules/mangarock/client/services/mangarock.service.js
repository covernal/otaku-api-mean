(function () {
  'use strict';

  // Users service used for communicating with the users REST endpoint
  angular
    .module('mangarock.services')
    .factory('MangarockService', MangarockService);

  MangarockService.$inject = ['$http'];

  function MangarockService($http) {
    $http.defaults.headers.post['Content-Type'] = 'applicaiton/x-www-form-urlencoded';
    delete $http.defaults.headers.common['X-Requested-With'];
    $http.defaults.useXDomain = true;
    var baseURL = '/mr';
    var service = {
      getAllMangas: getAllMangas,
      getAllAuthors: getAllAuthors,
      getDetails: getDetails
    };

    return service;

    function getAllMangas(cb) {
      var url = baseURL + '/query/web400/mrs_filter?country=United States';
      var data = {
        status: 'all',
        genres: {},
        rank: 'all',
        order: 'name'
      };
      $http.post(
        url,
        data,
        {
          headers: {
            'Content-Type': 'application/json'
          }
        }
      ).then(function (response) {
        cb(null, response);
      }, function (err) {
        cb(err);
      });
    }

    function getAllAuthors(cb) {
      var url = baseURL + '/query/web400/mrs_all_author?country=United%20States';
      var data = {
        'name': ''
      };
      $http.post(
        url,
        data
      ).then(function (response) {
        cb(null, response);
      }, function (err) {
        cb(err);
      });
    }

    function getDetails(oids, cb) {
      var url = baseURL + '/meta';
      var data = oids;
      $http.post(
        url,
        data
      ).then(function (response) {
        cb(null, response);
      }, function (err) {
        cb(err);
      });
    }
  }
}());
