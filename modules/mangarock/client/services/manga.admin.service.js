(function () {
  'use strict';

  // Users service used for communicating with the users REST endpoint
  angular
    .module('mangarock.admin.services')
    .factory('MangaAdminService', MangaAdminService);

  MangaAdminService.$inject = ['$http'];

  function MangaAdminService($http) {
    var baseURL = '/api/adm';
    var service = {
      getTasks: getTasks,
      commandTask: commandTask,
      dashInfo: dashInfo,
      systemData: systemData,
      writeSystemData: writeSystemData
    };

    return service;

    function getTasks(cb) {
      var url = baseURL + '/scrap/tasks';
      $http.get(
        url,
        {
          headers: {
            'Content-Type': 'application/json'
          }
        }
      ).then(function (response) {
        cb(null, response);
      }, function (err) {
        cb(err);
      });
    }

    function commandTask(script, command, cb) {
      var url = baseURL + '/scrap/' + script + '/' + command;
      $http.get(
        url,
        {
          headers: {
            'Content-Type': 'application/json'
          }
        }
      ).then(function (response) {
        cb(null, response);
      }, function (err) {
        cb(err);
      });
    }

    function dashInfo(cb) {
      var url = baseURL + '/dash/info';
      $http.get(
        url,
        {
          headers: {
            'Content-Type': 'application/json'
          }
        }
      ).then(function (response) {
        cb(null, response);
      }, function (err) {
        cb(err);
      });
    }

    function systemData(cb) {
      var url = baseURL + '/../system/data';
      $http.get(
        url,
        {
          headers: {
            'Content-Type': 'application/json'
          }
        }
      ).then(function (response) {
        cb(null, response);
      }, function (err) {
        cb(err);
      });
    }

    function writeSystemData(data, cb) {
      var url = baseURL + '/data/set';
      $http.post(
        url,
        data,
        {
          headers: {
            'Content-Type': 'application/json'
          }
        }
      ).then(function (response) {
        cb(null, response);
      }, function (err) {
        cb(err);
      });
    }
  }
}());
