(function () {
  'use strict';

  angular
    .module('mangarock')
    .controller('MangaImportController', MangaImportController);

  MangaImportController.$inject = ['$scope', 'Authentication', 'MangaAdminService', 'MangarockService', 'Notification'];

  function MangaImportController($scope, Authentication, MangaAdminService, MangarockService, Notification) {
    var vm = this;

    vm.user = Authentication.user;
    vm.tasks = [{
      name: 'scrap',
      description: 'scrap manga, author, characters from Mangarock',
      status: undefined
    }, {
      name: 'scrap_collection',
      description: 'scrap collection from Mangarock',
      status: undefined
    }];

    MangaAdminService.getTasks(function (err, response) {
      if (err || response.status != 200) {
        vm.taskstatus = {};
      }
      response = response.data;
      if (!response.code) {
        vm.taskstatus = {};
      }
      vm.tasks.forEach(function(task) {
        task.status = response.data[task.name];
      });
      console.log(vm.tasks);
    });

    vm.doAction = function (task) {
      var command = ''
      if (task.status) {
        command = 'stop';
        console.log('script will be stopped');
      } else {
        command = 'start'
        console.log('script will be began');
      }
      MangaAdminService.commandTask(task.name, command, function (err, response) {
        if (err || response.status != 200) {
          vm.taskstatus = {};
        }
        response = response.data;
        if (!response.code) {
          vm.taskstatus = {};
        }
        vm.tasks.forEach(function(task) {
          task.status = response.data[task.name];
        });
      });
    }

  }
}());
