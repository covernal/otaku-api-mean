(function () {
  'use strict';

  angular
    .module('mangarock', ['chart.js'])
    .config(['ChartJsProvider', function (ChartJsProvider) {
      // Configure all charts
      // ChartJsProvider.setOptions({
      //   chartColors: [ '#803690', '#00ADF9', '#DCDCDC', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360'],
      //   responsive: false
      // });
      ChartJsProvider.setOptions('bar', {
        scales: {
          yAxes: [
            {
              ticks: { beginAtZero: true, suggestedMax: 10 }
            }
          ]
        },
        // "animation": {
        //   "duration": 1,
        //   "onComplete": function() {
        //     var chartInstance = this.chart,
        //       ctx = chartInstance.ctx;
    
        //     ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
        //     ctx.textAlign = 'center';
        //     ctx.textBaseline = 'bottom';
    
        //     this.data.datasets.forEach(function(dataset, i) {
        //       var meta = chartInstance.controller.getDatasetMeta(i);
        //       meta.data.forEach(function(bar, index) {
        //         var data = dataset.data[index];
        //         ctx.fillText(data, bar._model.x, bar._model.y - 5);
        //       });
        //     });
        //   }
        // }
      });
    }])
    .controller('MangaHomeController', MangaHomeController);

    MangaHomeController.$inject = ['$scope', 'Authentication', 'MangaAdminService', 'MangarockService'];

  function MangaHomeController($scope, Authentication, MangaAdminService, MangarockService) {
    var vm = this;

    vm.user = Authentication.user;

    $scope.colours = [{
      backgroundColor: "#4488FF",
      borderColor: "#4488FF",
    }];
    $scope.labels = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    $scope.user_count = {
      "series": ["Users"],
      "data": [["0", "0", "0", "0", "0", "0", "0", "0", "0", "0"]],
    };
    $scope.manga_count = {
      "series": ["Mangas"],
      "data": [["0", "0", "0", "0", "0", "0", "0", "0", "0", "0"]],
    };
    $scope.chapter_count = {
      "series": ["Chapters"],
      "data": [["0", "0", "0", "0", "0", "0", "0", "0", "0", "0"]],
    };

    var d = new Date();
    var y = d.getFullYear();
    var m = d.getMonth() + 1;
    $scope.labels = $scope.labels.slice(m-12).concat($scope.labels.slice(0, m%12));
    for(var i = 11; i >= 0; i--) {
      $scope.labels[i] = $scope.labels[i] + ' / ' + y
      if (m-- === 1) {
        y--;
        m = 12;
      }
    }

    MangaAdminService.dashInfo(function (err, response) {
      if (err || response.status != 200) {
        vm.data = {};
        // return;
      }
      response = response.data;
      if (!response.code) {
        vm.data = {};
        // return;
      }
      vm.data = response.data;
      vm.data.user.statistic = rearrange_statistic(vm.data.user.statistic);
      vm.data.manga.statistic = rearrange_statistic(vm.data.manga.statistic);
      vm.data.chapter.statistic = rearrange_statistic(vm.data.chapter.statistic);

      var d = new Date();
      var y = d.getFullYear();
      var m = d.getMonth() + 1;
      for(var i = 11; i >= 0; i--) {        
        var _m = vm.data.user.statistic[y] ? vm.data.user.statistic[y].months[m] : undefined;
        $scope.user_count.data[0][i] =  _m ? _m.count : 0;
        if (m-- === 1) {
          y--; m = 12;
        }
      }

      y = d.getFullYear();
      m = d.getMonth() + 1;
      for(var i = 11; i >= 0; i--) {        
        var _m = vm.data.manga.statistic[y] ? vm.data.manga.statistic[y].months[m] : undefined;
        $scope.manga_count.data[0][i] =  _m ? _m.count : 0;
        if (m-- === 1) {
          y--; m = 12;
        }
      }

      y = d.getFullYear();
      m = d.getMonth() + 1;
      for(var i = 11; i >= 0; i--) {        
        var _m = vm.data.chapter.statistic[y] ? vm.data.chapter.statistic[y].months[m] : undefined;
        $scope.chapter_count.data[0][i] =  _m ? _m.count : 0;
        if (m-- === 1) {
          y--; m = 12;
        }
      }
    });

    function rearrange_statistic(statistic) {
      var years = {
        count: 0
      };
      statistic.forEach(item => {
        var _year = {
          count: 0,
          months: {}
        };
        years[item._id.year] = _year;        
        item.monthlycount.forEach(item => {
          var _month = {
            count: 0,
            days: {}
          };
          _year.months[item.month] = _month;
          item.dailycount.forEach(item => {
            _month.days[item.day] = item.count;
            _month.count += item.count;
          });
          _year.count += _month.count;
        });
        console.log(years.count, _year.count);
        years.count += _year.count;
        console.log('=', years.count);
      });
      console.log('=', years.count);
      return years;
    }

    MangaAdminService.systemData(function (err, response) {
      if (err || response.status != 200) {
        return;
      }

      var response = response.data;
      vm.dpp = response.data.DisplayPerPages;

      console.log(response)
    });

    vm.dpp = 10;
    vm.onChangeDPP = function () {
      console.log(vm.dpp);
      var dpp = isNaN(vm.dpp) || vm.dpp < 1 ? 10 : vm.dpp;
      dpp = Math.round(dpp);
      vm.dpp = dpp;
      
    }

    vm.onSave = function () {
      console.log(vm.dpp);

      MangaAdminService.writeSystemData({
        DisplayPerPages: vm.dpp
      }, function (err, res) {
        console.log(err, res);
      })
    }

  }
}());
