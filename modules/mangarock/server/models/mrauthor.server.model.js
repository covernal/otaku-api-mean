'use strict';

/**
 * Module dependencies.
 * Refer to https://www.mangaeden.com/api/
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Mgeden Schema
 */
var MrAuthorSchema = new Schema({
  oid: {
    type: String,
    unique: true
  },
  name: {
    type: String,
    default: '',
    required: 'Please fill Author name',
    trim: true
  },
  lowerName: {
    type: String
  },
  bio: {
    type: String,
  },
  thumbnail: {
    type: String,
  },
  artworks: [String],
  extra: {},
  
  // inserted date
  created: {
    type: Date,
    default: Date.now
  }
  // // user who inserted
  // user: {
  //   type: Schema.ObjectId,
  //   ref: 'User'
  // }
});

// EXPOSE ONLY CERTAIN FIELDS
// It's really important that we keep
// stuff like password private!
MrAuthorSchema.method('toJSON', function () {
  return {
    oid: this.oid,
    name: this.name,
    bio: this.bio || '',
    thumbnail: this.thumbnail || '',
    artworks: this.artworks,
    extra: this.extra
  };
});


MrAuthorSchema.statics.seed = seed;

mongoose.model('MrAuthor', MrAuthorSchema);

/**
* Seeds the MrAuthor collection with document (User)
* and provided options.
*/
function seed(doc, options) {
  // console.log(doc);
  var MrAuthor = mongoose.model('MrAuthor');

  return new Promise(function (resolve, reject) {

    skipDocument()
      .then(add)
      .then(function (response, result) {
        return resolve(response, result);
      })
      .catch(function (err) {
        return reject(err);
      });

    function skipDocument() {
      return new Promise(function (resolve, reject) {
        MrAuthor
          .findOne({
            oid: doc.oid
          })
          .exec(function (err, existing) {
            if (err) {
              return reject(err);
            }

            if (!existing) {
              return resolve(false);
            }

            if (existing && !options.overwrite) {
              return resolve(true);
            }

            // Remove MrAuthor (overwrite)

            existing.remove(function (err) {
              if (err) {
                return reject(err);
              }

              return resolve(false);
            });
          });
      });
    }

    function add(skip) {
      return new Promise(function (resolve, reject) {

        if (skip) {
          return resolve({
            message: chalk.yellow('Database Seeding: Genere\t\t' + doc.name + ' skipped')
          }, result);
        }

        var author = new MrAuthor(doc);        

        author.save(function (err, result) {
          if (err) {
            console.log(err);
            return reject(err);
          }

          return resolve({
            message: 'Database Seeding: MrAuthor\t\t' + author.name + ' added',
            added: result
          });
        });        
      });
    }

  });
}
