'use strict';

/**
 * Module dependencies.
 * Refer to https://www.mangaeden.com/api/
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Mgeden Schema
 */
var MrCollectionSchema = new Schema({
  oid: {
    type: String,
    unique: true
  },
  name: {
    type: String,
    default: '',
    required: 'Please fill Author name',
    trim: true
  },
  lowerName: String,
  description: String,
  banner: String,
  header_type: String,
  header_data: {},
  item_layout: String,
  item_type: String,
  items: [{
    oid: String,
    order: Number,
    data: {}
  }],
  
  // inserted date
  updated_at: {
    type: Date,
    default: Date.now
  }
});

MrCollectionSchema.method('toJSON', function () {
  return {
    oid: this.oid,
    name: this.name,
    description: this.description,
    banner: this.banner,
    header_type: this.header_type,
    header_data: this.header_data,
    item_layout: this.item_layout,
    item_type: this.item_type,
    items: this.items.map(function (m) {
      return {
        oid: m.oid,
        order: m.order,
        data: m.data
      }
    })
  };
});

MrCollectionSchema.pre('save', function(next) {
  this.lowerName = this.name.toLowerCase();
  this.updated_at = Date.now();
  next();
});

MrCollectionSchema.statics.seed = seed;

mongoose.model('MrCollection', MrCollectionSchema);

/**
* Seeds the MrCollection collection with document (User)
* and provided options.
*/
function seed(doc, options) {
  // console.log(doc);
  var MrCollection = mongoose.model('MrCollection');

  return new Promise(function (resolve, reject) {

    skipDocument()
      .then(add)
      .then(function (response, result) {
        return resolve(response, result);
      })
      .catch(function (err) {
        return reject(err);
      });

    function skipDocument() {
      return new Promise(function (resolve, reject) {
        MrCollection
          .findOne({
            oid: doc.oid
          })
          .exec(function (err, existing) {
            if (err) {
              return reject(err);
            }

            if (!existing) {
              return resolve(false);
            }

            if (existing && !options.overwrite) {
              return resolve(true);
            }

            // Remove MrCollection (overwrite)

            existing.remove(function (err) {
              if (err) {
                return reject(err);
              }

              return resolve(false);
            });
          });
      });
    }

    function add(skip) {
      return new Promise(function (resolve, reject) {

        if (skip) {
          return resolve({
            message: chalk.yellow('Database Seeding: Genere\t\t' + doc.name + ' skipped')
          }, result);
        }

        var collection = new MrCollection(doc);        

        collection.save(function (err, result) {
          if (err) {
            console.log(err);
            return reject(err);
          }

          return resolve({
            message: 'Database Seeding: MrCollection\t\t' + collection.name + ' added',
            added: result
          });
        });        
      });
    }

  });
}
