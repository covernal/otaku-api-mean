'use strict';

/**
 * Module dependencies.
 * Refer to https://www.mangaeden.com/api/
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Mgeden Schema
 */
var MrSectionSchema = new Schema({
  oid: {
    type: String,
    unique: true
  },
  title: {
    type: String,
    default: '',
    required: 'Please fill Author name',
    trim: true
  },
  subtitle: String,
  type: String,
  items: {
    component: String,
    data: [{
      type: Schema.ObjectId,
      ref: 'MrCollection'
    }]
  },
  
  // inserted date
  updated_at: {
    type: Date,
    default: Date.now
  }
});

MrSectionSchema.pre('save', function(next) {
  this.updated_at = Date.now();
  next();
});

// EXPOSE ONLY CERTAIN FIELDS
// It's really important that we keep
// stuff like password private!
MrSectionSchema.method('toJSON', function () {
  return {
    sectionId: this.oid,
    title: {
      en: this.title
    },
    subtitle: {
      en: this.subtitle
    },
    type: this.type,
    items: this.items
  };
});


MrSectionSchema.statics.seed = seed;

mongoose.model('MrSection', MrSectionSchema);

/**
* Seeds the MrSection collection with document (User)
* and provided options.
*/
function seed(doc, options) {
  // console.log(doc);
  var MrSection = mongoose.model('MrSection');

  return new Promise(function (resolve, reject) {

    skipDocument()
      .then(add)
      .then(function (response, result) {
        return resolve(response, result);
      })
      .catch(function (err) {
        return reject(err);
      });

    function skipDocument() {
      return new Promise(function (resolve, reject) {
        MrSection
          .findOne({
            oid: doc.oid
          })
          .exec(function (err, existing) {
            if (err) {
              return reject(err);
            }

            if (!existing) {
              return resolve(false);
            }

            if (existing && !options.overwrite) {
              return resolve(true);
            }

            // Remove MrSection (overwrite)

            existing.remove(function (err) {
              if (err) {
                return reject(err);
              }

              return resolve(false);
            });
          });
      });
    }

    function add(skip) {
      return new Promise(function (resolve, reject) {

        if (skip) {
          return resolve({
            message: chalk.yellow('Database Seeding: Section\t\t' + doc.name + ' skipped')
          }, result);
        }

        var collection = new MrSection(doc);        

        collection.save(function (err, result) {
          if (err) {
            console.log(err);
            return reject(err);
          }

          return resolve({
            message: 'Database Seeding: MrCollection\t\t' + collection.title + ' added',
            added: result
          });
        });        
      });
    }

  });
}
