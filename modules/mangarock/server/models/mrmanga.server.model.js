'use strict';

/**
 * Module dependencies.
 * Refer to https://www.mangaeden.com/api/
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Mgeden Schema
 */
var MrMangaSchema = new Schema({
  // identity number from MangaEden - i
  oid: {
    type: String,
    unique: true
  },
  name: {
    type: String
  },
  lowerName: {
    type: String
  },
  rank: {
    type: Number,
    default: 0
  },
  mid: {
    type: Number,
    default: 0
  },
  msid: {
    type: Number,
    default: 0
  },
  direction: {
    type: Number,
    default: 0
  },

  completed: {
    type: Boolean,
    default: false
  },  
  last_updated: {
    type: Date,
    default: Date.now
  },
  removed: {
    type: Boolean,
    default: false
  },

  categories: [{
    type: Schema.ObjectId,
    ref: 'MrGenre'
  }],
  chapters:[{
    type: Schema.ObjectId,
    ref: 'MrChapter'
  }],
  characters: [{
    type: Schema.ObjectId,
    ref: 'MrAuthor'
  }],
  authors: [{
    type: Schema.ObjectId,
    ref: 'MrAuthor'
  }],
  description: {
    type: String,
    default: ''
  },

  thumbnail: {
    type: String
  },
  cover: {
    type: String
  },
  artworks: [ String ],
  alias: [ String ],
  extra: Schema.Types.Mixed,

  // inserted date
  created: {
    type: Date,
    default: Date.now
  }
});

// EXPOSE ONLY CERTAIN FIELDS
// It's really important that we keep
// stuff like password private!
MrMangaSchema.method('toJSON', function () {
  return {
    id: this._id,
    oid: this.oid,
    mid: this.mid,
    name: this.name,
    author: (this.authors || []).length == 0 ? '' : this.authors[0].name,
    msid: this.msid,
    rank: this.rank,
    completed: this.completed,
    last_updated: this.last_updated,
    removed: this.removed,
    direction: this.direction,
    total_chapters: (this.chapters || []).length,
    description: this.description,
    // categories: this.categories,
    chapters: this.chapters,
    thumbnail: this.thumbnail,
    cover: this.cover,
    artworks: this.artworks,
    alias: this.alias,
    characters: this.characters  || [],
    authors: this.authors || [],
    rich_categories: this.categories,
    extra: this.extra
  };
});

MrMangaSchema.statics.seed = seed;

mongoose.model('MrManga', MrMangaSchema);

/**
* Seeds the MrManga collection with document (User)
* and provided options.
*/
function seed(doc, options) {
  var MrManga = mongoose.model('MrManga');

  return new Promise(function (resolve, reject) {

    skipDocument()
      .then(add)
      .then(function (response, doc) {
        return resolve(response, doc);
      })
      .catch(function (err) {
        return reject(err);
      });

    function skipDocument() {
      return new Promise(function (resolve, reject) {
        MrManga
          .findOne({
            oid: doc.oid
          })
          .exec(function (err, existing) {
            if (err) {
              return reject(err);
            }

            if (!existing) {
              return resolve(false);
            }

            if (existing && !options.overwrite) {
              return resolve(true);
            }

            // Remove MrManga (overwrite)
            existing.remove(function (err) {
              if (err) {
                return reject(err);
              }

              return resolve(false);
            });
          });
      });
    }

    function add(skip) {
      return new Promise(function (resolve, reject) {

        if (skip) {
          return resolve({
            message: chalk.yellow('Database Seeding: Genere\t\t' + doc.name + ' skipped')
          });
        }

        if (!doc.oid || doc.oid === '') {
          return reject('oid can\'t be set null or empty');
        }

        var manga = new MrManga(doc);

        manga.save(function (err, result) {
          if (err) {
            return reject(err);
          }

          return resolve({
            message: 'Database Seeding: Manga\t\t' + manga.name + ' added',
            added: result
          });
        });        
      });
    }

  });
}
