'use strict';

/**
 * Module dependencies.
 * Refer to https://www.mangaeden.com/api/
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Mgeden Schema
 */
var MrTrackingSchema = new Schema({
  oid: {
    type: String,
    unique: true
  },  
  sections: [{
    type: Schema.ObjectId,
    ref: 'MrSection'
  }],
  
  // inserted date
  updated_at: {
    type: Date,
    default: Date.now
  }
});

MrTrackingSchema.pre('save', function(next) {
  this.updated_at = Date.now();
  next();
});

// EXPOSE ONLY CERTAIN FIELDS
// It's really important that we keep
// stuff like password private!
MrTrackingSchema.method('toJSON', function () {
  return {
    trackingId: this.oid,
    sections: this.sections
  };
});


MrTrackingSchema.statics.seed = seed;

mongoose.model('MrTracking', MrTrackingSchema);

/**
* Seeds the MrTracking collection with document (User)
* and provided options.
*/
function seed(doc, options) {
  // console.log(doc);
  var MrTracking = mongoose.model('MrTracking');

  return new Promise(function (resolve, reject) {

    skipDocument()
      .then(add)
      .then(function (response, result) {
        return resolve(response, result);
      })
      .catch(function (err) {
        return reject(err);
      });

    function skipDocument() {
      return new Promise(function (resolve, reject) {
        MrTracking
          .findOne({
            oid: doc.oid
          })
          .exec(function (err, existing) {
            if (err) {
              return reject(err);
            }

            if (!existing) {
              return resolve(false);
            }

            if (existing && !options.overwrite) {
              return resolve(true);
            }

            // Remove MrTracking (overwrite)

            existing.remove(function (err) {
              if (err) {
                return reject(err);
              }

              return resolve(false);
            });
          });
      });
    }

    function add(skip) {
      return new Promise(function (resolve, reject) {

        if (skip) {
          return resolve({
            message: chalk.yellow('Database Seeding: Section\t\t' + doc.name + ' skipped')
          }, result);
        }

        var collection = new MrTracking(doc);        

        collection.save(function (err, result) {
          if (err) {
            console.log(err);
            return reject(err);
          }

          return resolve({
            message: 'Database Seeding: MrTracking\t\t' + collection.oid + ' added',
            added: result
          });
        });        
      });
    }

  });
}
