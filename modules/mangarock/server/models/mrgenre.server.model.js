'use strict';

/**
 * Module dependencies.
 * Refer to https://www.mangaeden.com/api/
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Mgeden Schema
 */
var MrGenreSchema = new Schema({
  oid: {
    type: String
  },
  name: {
    type: String,
    required: true,
    trim: true
  }.type,
  cover: {
    type: String
  },
  description: {
    type: String
  }
});

// EXPOSE ONLY CERTAIN FIELDS
// It's really important that we keep
// stuff like password private!
MrGenreSchema.method('toJSON', function () {
  return {
    oid: this.oid,
    name: this.name
  };
});


MrGenreSchema.statics.seed = seed;

mongoose.model('MrGenre', MrGenreSchema);

/**
* Seeds the MrGenre collection with document (User)
* and provided options.
*/
function seed(doc, options) {
  var MrGenre = mongoose.model('MrGenre');

  return new Promise(function (resolve, reject) {

    skipDocument()
      .then(add)
      .then(function (response) {
        return resolve(response);
      })
      .catch(function (err) {
        return reject(err);
      });

    function skipDocument() {
      return new Promise(function (resolve, reject) {
        MrGenre
          .findOne({
            _id: doc._id
          })
          .exec(function (err, existing) {
            if (err) {
              return reject(err);
            }

            if (!existing) {
              return resolve(false);
            }

            if (existing && !options.overwrite) {
              return resolve(true);
            }

            // Remove MrGenre (overwrite)

            existing.remove(function (err) {
              if (err) {
                return reject(err);
              }

              return resolve(false);
            });
          });
      });
    }

    function add(skip) {
      return new Promise(function (resolve, reject) {

        if (skip) {
          return resolve({
            message: chalk.yellow('Database Seeding: Genere\t\t' + doc.name + ' skipped')
          });
        }

        var genre = new MrGenre(doc);

        genre.save(function (err, result) {
          if (err) {
            return reject(err);
          }

          return resolve({
            message: 'Database Seeding: Genre\t\t' + genre.name + ' added',
            added: result
          });
        });        
      });
    }

  });
}
