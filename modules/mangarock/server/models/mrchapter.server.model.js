'use strict';

/**
 * Module dependencies.
 * Refer to https://www.mangaeden.com/api/
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Mgeden Schema
 */
var MrChapterSchema = new Schema({
  manga_oid: {
    type: String
  },
  oid: {
    type: String,
    unique: true
  },
  cid: {
    type: Number
  },
  name: {
    type: String,
    default: '',
    required: 'Please fill Chapter name',
    trim: true
  },
  order: {
    type: Number
  },
  
  // inserted date
  updatedAt: {
    type: Date,
    default: Date.now
  },
  pages: [String]
});

// EXPOSE ONLY CERTAIN FIELDS
// It's really important that we keep
// stuff like password private!
MrChapterSchema.method('toJSON', function () {
  return {
    oid: this.oid,
    cid: this.cid,
    order: this.order,
    name: this.name,
    updatedAt: this.updatedAt ? Math.floor(new Date(this.updatedAt).getTime() / 1000) : 0
  };
});


MrChapterSchema.statics.seed = seed;

mongoose.model('MrChapter', MrChapterSchema);

/**
* Seeds the MrChapter collection with document (User)
* and provided options.
*/
function seed(doc, options) {
  var MrChapter = mongoose.model('MrChapter');

  return new Promise(function (resolve, reject) {

    skipDocument()
      .then(add)
      .then(function (response, result) {
        return resolve(response, result);
      })
      .catch(function (err) {
        return reject(err);
      });

    function skipDocument() {
      return new Promise(function (resolve, reject) {
        MrChapter
          .findOne({
            oid: doc.oid
          })
          .exec(function (err, existing) {
            if (err) {
              return reject(err);
            }

            if (!existing) {
              return resolve(false);
            }

            if (existing && !options.overwrite) {
              return resolve(true);
            }

            // Remove MrChapter (overwrite)

            existing.remove(function (err) {
              if (err) {
                return reject(err);
              }

              return resolve(false);
            });
          });
      });
    }

    function add(skip) {
      return new Promise(function (resolve, reject) {

        if (skip) {
          return resolve({
            message: chalk.yellow('Database Seeding: Chapter\t\t' + doc.name + ' skipped')
          });
        }

        var chapter = new MrChapter(doc);   
        
        chapter.updatedAt = new Date().toISOString();

        chapter.save(function (err, result) {
          if (err) {
            return reject(err);
          }

          return resolve({
            message: 'Database Seeding: MrChapter\t\t' + chapter.name + ' added',
            added: result
          });
        });        
      });
    }
  });
}
