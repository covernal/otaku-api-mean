'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  pm2 = require('pm2'),
  async = require('async'),
  hash = require('object-hash'),
  axios = require('axios'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Mgeden = mongoose.model('Mgeden'),
  MrManga = mongoose.model('MrManga'),
  MrChapter = mongoose.model('MrChapter'),
  MrGenre = mongoose.model('MrGenre'),
  MrAuthor = mongoose.model('MrAuthor'),
  MrTracking = mongoose.model('MrTracking'),
  MrSection = mongoose.model('MrSection'),
  MrCollection = mongoose.model('MrCollection'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

exports.pm2list = function (req, res) {
  pm2.list(function (err, processDescriptionList) {
    if (err) {
      return res.json({
        'code': 100,
        'data': 'Unknown error'
      });
    } else {
      var tasks = {};
      processDescriptionList.forEach(proc => {
        tasks[proc.name] = proc.pm2_env.status;
      });
      res.json({
        'code': 0,
        'data': tasks
      });
    }
  })
};

exports.pm2command = function (req, res) {
  var map = {
    'scrap': './scripts/scrap.js',
    'scrap_collection': './scripts/scrap-collection.json',
  }
  var command = req.params.command;
  var script = req.params.script;
  if (command === 'start') {
    pm2.start(map[script], function (err, proc) {
      exports.pm2list(req, res); 
    });
  } else if (command === 'stop') {
    pm2.delete(map[script], function (err, proc) {
      exports.pm2list(req, res); 
    });
  }
};

exports.dashinfo = function (req, res) {
  var data = {};
  async.waterfall([
    function (done) {
      User
        .count({  })
        .exec(function (err, count) {
          if (err) {
            return done(100, 'Unknown error');
          } else {
            data.user = {};
            data.user.count = count
            done(null, data);
          }
        });
    },
    function (data, done) {
      MrManga
        .count({  })
        .exec(function (err, count) {
          if (err) {
            return done(100, 'Unknown error');
          } else {
            data.manga = {};
            data.manga.count = count
            done(null, data);
          }
        });
    },
    function (data, done) {
      MrChapter
        .count({  })
        .exec(function (err, count) {
          if (err) {
            return done(100, 'Unknown error');
          } else {
            data.chapter = {};
            data.chapter.count = count
            done(null, data);
          }
        });
    },
    function (data, done) {
      User.aggregate(
        { $group : { 
          _id : { year: { $year : "$created" }, month: { $month : "$created" },day: { $dayOfMonth : "$created" }}, 
          count : { $sum : 1 }}
        }, 
        { $group : { 
          _id : { year: "$_id.year", month: "$_id.month" }, 
          dailycount: { $push: { day: "$_id.day", count: "$count" }}}
        }, 
        { $group : { 
          _id : { year: "$_id.year" }, 
          monthlycount: { $push: { month: "$_id.month", dailycount: "$dailycount" }}}
        }, 
        function (err, res) {
          if (err) ; // TODO handle error 
          console.log(res); 
          data.user.statistic = res;
          done(null, data);
        }
      );
    },
    function (data, done) {
      MrManga.aggregate(
        { $group : { 
          _id : { year: { $year : "$created" }, month: { $month : "$created" },day: { $dayOfMonth : "$created" }}, 
          count : { $sum : 1 }}
        }, 
        { $group : { 
          _id : { year: "$_id.year", month: "$_id.month" }, 
          dailycount: { $push: { day: "$_id.day", count: "$count" }}}
        }, 
        { $group : { 
          _id : { year: "$_id.year" }, 
          monthlycount: { $push: { month: "$_id.month", dailycount: "$dailycount" }}}
        }, 
        function (err, res) {
          if (err) ; // TODO handle error 
          console.log(res); 
          data.manga.statistic = res;
          done(null, data);
        }
      );
    },
    function (data, done) {
      MrChapter.aggregate(
        { $group : { 
          _id : { year: { $year : "$updatedAt" }, month: { $month : "$updatedAt" },day: { $dayOfMonth : "$updatedAt" }}, 
          count : { $sum : 1 }}
        }, 
        { $group : { 
          _id : { year: "$_id.year", month: "$_id.month" }, 
          dailycount: { $push: { day: "$_id.day", count: "$count" }}}
        }, 
        { $group : { 
          _id : { year: "$_id.year" }, 
          monthlycount: { $push: { month: "$_id.month", dailycount: "$dailycount" }}}
        }, 
        function (err, res) {
          if (err) ; // TODO handle error 
          console.log(res); 
          data.chapter.statistic = res;
          done(null, data);
        }
      );
    },
    function (data, done) {
      console.log('dash info:', data);
      res.json({
        code: 0,
        data: data
      });
    }
  ], function (err, result) {
    res.json({
      'code': err,
      'data': result
    });
  });
}