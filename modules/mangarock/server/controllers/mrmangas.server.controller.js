'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  async = require('async'),
  hash = require('object-hash'),
  axios = require('axios'),
  mongoose = require('mongoose'),
  Mgeden = mongoose.model('Mgeden'),
  MrManga = mongoose.model('MrManga'),
  MrChapter = mongoose.model('MrChapter'),
  MrGenre = mongoose.model('MrGenre'),
  MrAuthor = mongoose.model('MrAuthor'),
  MrTracking = mongoose.model('MrTracking'),
  MrSection = mongoose.model('MrSection'),
  MrCollection = mongoose.model('MrCollection'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

exports.genre = function (req, res) {
  var oid = req.query.oid;
  if (!oid) {
    return res.json({
      'code': 103,
      'data': 'Unknown author'
    });
  } else {
    async.waterfall([
      function (done) {
        MrGenre
          .findOne({ oid: oid })
          .exec(function (err, genre) {
            if (err) {
              return done(100, 'Unknown error');
            } else if (!genre) {
              return done(103, 'Unknown genre');
            } else {
              done(null, genre);
            }
          });
      },
      function (genre, done) {
        MrManga
          .find({ categories: { $all: [genre] } })
          .exec(function (err, mangas) {
            if (err) {
              return done(10, 'Unknown error');
            } else {
              var result = [];
              (mangas || []).forEach(function (m) {
                result.push(m.oid);
              });
              result = {
                oid: genre.oid,
                name: genre.name,
                description: genre.description,
                cover: genre.cover,
                series: result
              }
              done(null, result);
            }
          });
      },
      function (mangas, done) {
        console.log('done');
        res.json({
          code: 0,
          data: mangas
        });
      }
    ], function (err, result) {
      res.json({
        'code': err,
        'data': result
      });
    });
  }
}

exports.pages = function (req, res) {
  var oid = req.query.oid;
  if (!oid) {
    res.json({
      'code': 106,
      'data': 'Unknown chapter'
    });
  } else {
    MrChapter.findOne({ oid: oid }, function (err, chapter) {
      if (err) {
        res.json({
          'code': 100,
          'data': 'Unknown error'
        });
      } else {
        res.json({
          'code': 0,
          'data': chapter.pages
        });
      }
    });
  }
};

exports.manga = function (req, res) {
  var oid = req.query.oid;
  if (!oid) {
    res.json({
      'code': 103,
      'data': 'Unknown serie'
    });
  } else {
    MrManga
      .findOne({ oid: oid })
      .populate({ path: 'chapters', select: 'cid oid order name updatedAt' })
      .populate('categories')
      .populate({ path: 'characters', select: 'name oid thumbnail'})
      .populate({ path: 'authors', select: 'oid name thumbnail -_id' })
      .exec(function (err, manga) {
        if (err) {
          res.json({
            'code': 100,
            'data': 'Unknown error'
          });
        } else {
          res.json({
            'code': 0,
            'data': manga
          });
        }
      });
  }
};

exports.multiManga = function (req, res) {
  var rawOids = req.body.toString() || '';
  rawOids = rawOids.split(';');

  var oids = [];
  rawOids.forEach(function (ro) {
    oids.push(ro.replace(':0', ''));
  });
  
  MrManga
    .find({ oid: { $in: oids } })
    .populate({ path: 'chapters', select: 'cid oid order name updatedAt' })
    .populate('categories')
    .populate({ path: 'characters', select: 'name oid thumbnail'})
    .populate({ path: 'authors', select: 'oid name thumbnail -_id' })
    .exec(function (err, mangas) {
      if (err) {
        res.json({
          'code': 100,
          'data': 'Unknown error'
        });
      } else {
        var result = {};
        mangas.forEach(function (m) {
          result[m.mid] = {
            code: 0,
            data: m
          };
        });
        res.json({
          'code': 0,
          'data': result
        });
      }
    });
};

exports.character = function (req, res) {
  var oid = req.query.oid;
  if (!oid) {
    res.json({
      'code': 103,
      'data': 'Unknown author'
    });
  } else {
    MrAuthor
      .findOne({ oid: oid })
      .exec(function (err, author) {
        if (err) {
          res.json({
            'code': 100,
            'data': 'Unknown error'
          });
        } else {
          res.json({
            'code': 0,
            'data': author
          });
        }
      });
  }
};

exports.listMangaByCharacter = function (req, res) {
  var oid = req.query.oid;
  if (!oid) {
    return res.json({
      'code': 103,
      'data': 'Unknown author'
    });
  } else {
    async.waterfall([
      function (done) {
        MrAuthor
          .findOne({ oid: oid })
          .exec(function (err, character) {
            if (err) {
              return done(100, 'Unknown error');
            } else if (!character) {
              return done(103, 'Unknown character');
            } else {
              done(null, character);
            }
          });
      },
      function (character, done) {
        MrManga
          .find({ characters: { $all: [character] } })
          .exec(function (err, mangas) {
            if (err) {
              return done(10, 'Unknown error');
            } else {
              var result = [];
              (mangas || []).forEach(function (m) {
                result.push({
                  mid: m.mid,
                  oid: m.oid
                });
              });
              done(null, result);
            }
          });
      },
      function (mangas, done) {
        console.log('done');
        res.json({
          code: 0,
          data: mangas
        });
      }
    ], function (err, result) {
      res.json({
        'code': err,
        'data': result
      });
    });
  }
}

exports.author = function (req, res) {
  var oid = req.query.oid;
  if (!oid) {
    res.json({
      'code': 103,
      'data': 'Unknown author'
    });
  } else {
    MrAuthor
      .findOne({ oid: oid })
      .exec(function (err, author) {
        if (err) {
          res.json({
            'code': 100,
            'data': 'Unknown error'
          });
        } else {
          res.json({
            'code': 0,
            'data': author
          });
        }
      });
  }
};

exports.listMangaByAuthor = function (req, res) {
  var oid = req.query.oid;
  if (!oid) {
    return res.json({
      'code': 103,
      'data': 'Unknown author'
    });
  } else {
    async.waterfall([
      function (done) {
        MrAuthor
          .findOne({ oid: oid })
          .exec(function (err, author) {
            if (err) {
              return done(100, 'Unknown error');
            } else if (!author) {
              return done(103, 'Unknown author');
            } else {
              console.log(author);
              done(null, author);
            }
          });
      },
      function (author, done) {
        MrManga
          .find({ authors: { $all: [author] } })
          .exec(function (err, mangas) {
            if (err) {
              return done(10, 'Unknown error');
            } else {
              var result = [];
              (mangas || []).forEach(function (m) {
                result.push({
                  mid: m.mid,
                  oid: m.oid
                });
              });
              done(null, result);
            }
          });
      },
      function (mangas, done) {
        console.log('done');
        res.json({
          code: 0,
          data: mangas
        });
      }
    ], function (err, result) {
      res.json({
        'code': err,
        'data': result
      });
    });
  }
};

exports.listAuthorByName = function (req, res) {
  var data = req.body || {};
  var filter = data.name;
  if (filter === '') filter = undefined;

  var option = filter ? { 'lowerName': { '$regex': '^' + filter, '$options': 'i' } } : { };
  option.oid = { '$regex': 'mrs-author-', '$options': 'i' };
  MrAuthor
    .find(option)
    .sort({ 'lowerName': 1 })
    .exec(function (err, authors) {
      if (err) {
        return res.json({
          code: 100,
          data: 'Unknown error'
        });
      } else {
        var result = [];
        (authors || []).forEach(function (a) {
          result.push(a.oid);
        });
        return res.json({
          code: 0,
          data: result
        });
      }
    });
};

exports.similarMangas = function (req, res) {
  var mid = req.query.mid;
  if (!mid) {
    return res.json({
      'code': 103,
      'data': 'Unknown manga'
    });
  } else {
    async.waterfall([
      function (done) {
        MrManga
          .findOne({ mid: mid })
          .exec(function (err, manga) {
            if (err) {
              return done(100, 'Unknown error');
            } else if (!manga) {
              return done(103, 'Unknown manga');
            } else {
              done(null, manga);
            }
          });
      },
      function (manga, done) {
        MrManga
          .aggregate(
            { $match: { categories: { $in: manga.categories } } },
            { $unwind: '$categories' },
            { $match: { categories: { $in: manga.categories } } },
            { $group: {
              _id: '$_id',
              matches: { $sum: 1 }
            } },
            { $sort: { matches: -1 } },
            { $limit: 1000 }
          )
          .exec(function (err, mangaIds) {
            if (err) {
              console.log(err);
              return done(100, 'Unknown error');
            } else {
              done(null, manga, mangaIds || []);
            }
          });
      },
      function (manga, mangaIds, done) {
        var ids = [];
        var map = {};
        var reverseMap = {};
        var length = 0;
        mangaIds.forEach(function (m) {
          ids.push(m._id);
          map[m._id] = length;
          reverseMap[length] = m;
          length++;
        });
        MrManga
          .find({ _id: { $in: ids } })
          .populate('authors')
          .populate('categories', 'name')
          .exec(function (err, mangas) {
            if (err) {
              return done(100, 'Unknown error');
            } else {
              var result = [];
              (mangas || []).forEach(function (m) {
                var _m = m.toJSON();
                reverseMap[map[m._id]] = {
                  author: _m.author,
                  mid: _m.mid,
                  oid: _m.oid,
                  total_chapter: _m.total_chapters,
                  matches: reverseMap[map[m._id]].matches
                };
              });
              for (var i = 0; i < length; i++) {
                var m = reverseMap[i];
                if (m.oid === manga.oid) continue;
                result.push(m);
              }
              done(null, result);
            }
          });
      },
      function (mangas, done) {
        res.json({
          code: 0,
          data: mangas
        });
      }
    ], function (err, result) {
      res.json({
        'code': err,
        'data': result
      });
    });
  }
};

exports.meta = function (req, res) {
  var ids = req.body || [];
  async.waterfall([
    function (done) {
      MrManga
        .find({ oid: { $in: ids } })
        .select('authors completed name oid thumbnail chapters')
        .exec(function (err, mangas) {
          if (err) {
            return done(100, 'Unknown error');
          } else {
            var result = [];
            (mangas || []).forEach(function (m) {
              var _m = m.toJSON();
              result.push({
                author_dis: _m.authors,
                completed: _m.completed,
                name: _m.name,
                oid: _m.oid,
                thumbnail: _m.thumbnail,
                total_chapter: _m.total_chapters
              });
            });
            done(null, result);
          }
        });
    },
    function (result, done) {
      MrAuthor
        .find({ oid: { $in: ids } })
        .select('name oid thumbnail -_id')
        .exec(function (err, items) {
          if (err) {
            return done(100, 'Unknown error');
          } else {
            (items || []).forEach(function (ac) {
              result.push(ac);
            });
            done(null, result);
          }
        });
    },
    function (result, done) {
      MrChapter
        .find({ oid: { $in: ids } })
        .select('name oid -_id')
        .exec(function (err, chapters) {
          if (err) {
            return done(100, 'Unknown error');
          } else {
            (chapters || []).forEach(function (c) {
              result.push(c);
            });
            done(null, result);
          }
        });
    },
    function (mangas, done) {
      res.json({
        code: 0,
        data: mangas
      });
    }
  ], function (err, result) {
    res.json({
      'code': err,
      'data': result
    });
  });
};

exports.quicksearch = function (req, res) {
  var filter = req.body.toString() || '';
  return exports.searchBasic(res, filter, 3, 3, 3);
};

exports.search = function (req, res) {
  var params = req.body || {};
  var filter = params.keywords;
  var type = params.type;
  
  var limitManga = 0, limitAuthor = 0, limitCharacter = 0;
  if (type === 'series') limitManga = 9999999;
  else if (type === 'author') limitAuthor = 9999999;
  else if (type === 'character') limitCharacter = 9999999;
  else {
    return res.json({
      'code': 100,
      'data': 'Unknown error'
    });
  }

  return exports.searchBasic(res, filter, limitManga, limitAuthor, limitCharacter);
};

exports.searchBasic = function (res, filter, limitManga, limitAuthor, limitCharacter) {  
  // if (filter.length < 3) {
  //   return res.json({
  //     code: 0,
  //     data: {}
  //   });
  // }
  console.log(filter);
  async.waterfall([
    function (done) {
      if (limitManga < 1) return done(null, {});
      MrManga
        .find({
          lowerName: { '$regex': filter, '$options': 'i' }
        })
        .limit(limitManga)
        .exec(function (err, mangas) {
          if (err) {
            return done(100, 'Unknown error');
          } else {
            var result = {};
            var arr = [];
            (mangas || []).forEach(function (m) {
              arr.push(m.oid);
            });
            if (arr.length > 0) result.series = arr;
            done(null, result);
          }
        });
    },
    function (result, done) {
      if (limitAuthor < 1) return done(null, result);
      MrAuthor
        .find({
          lowerName: { '$regex': filter, '$options': 'i' },
          oid: { '$regex': 'mrs-author-', '$options': 'i' }

        })
        .limit(limitAuthor)
        .exec(function (err, items) {
          if (err) {
            return done(100, 'Unknown error');
          } else {
            var arr = [];
            (items || []).forEach(function (ac) {
              arr.push(ac.oid);
            });
            if (arr.length > 0) result.author = arr;
            done(null, result);
          }
        });
    },
    function (result, done) {
      if (limitCharacter < 1) return done(null, result);
      MrAuthor
        .find({
          lowerName: { '$regex': filter, '$options': 'i' },
          oid: { '$regex': 'mrs-character-', '$options': 'i' }

        })
        .limit(limitCharacter)
        .exec(function (err, items) {
          if (err) {
            return done(100, 'Unknown error');
          } else {
            var arr = [];
            (items || []).forEach(function (ac) {
              arr.push(ac.oid);
            });
            if (arr.length > 0) result.character = arr;
            done(null, result);
          }
        });
    },
    function (mangas, done) {
      res.json({
        code: 0,
        data: mangas
      });
    }
  ], function (err, result) {
    res.json({
      'code': err,
      'data': result
    });
  });
};

exports.filter = function (req, res) {
  var params = req.body || {};

  async.waterfall([
    function (done) {
      MrGenre.find({}, function (err, genres) {
        var genreMap = {};
        (genres || []).forEach(function (a) {
          genreMap[a.oid] = a;
        });
        done(null, genreMap);
      });
    },
    function (genreMap, done) {
      var options = {};
      var sort = undefined;

      if (params.status === 'completed') {
        options.completed = true;
      } else if (params.status === 'ongoing') {
        options.completed = false;
      } else if (params.status !== 'all') {
        return done(100, 'Unknown error - status');
      }

      if (params.order === 'rank') {
        sort = { 'rank': -1 };
      } else if (params.order === 'name') {
        sort = { 'lowerName': 1 };
      } else {
        return done(100, 'Unknown error - order');
      }

      if (params.rank !== 'all') {
        var els = params.rank.split('-');
        if (els.length !== 2) {
          return done(100, 'Unknown error - rank');
        }
        var from = parseInt(els[0]);
        var to = parseInt(els[1]);
        if (isNaN(from) || isNaN(to) && from >= to) {
          return done(100, 'Unknown error');
        }
        options.rank = { $gte: from, $lte: to };
      }

      var allowGenres = [];
      var denyGenres = [];
      Object.keys(params.genres).forEach(function (key) {
        if (!genreMap[key]) return;
        var value = params.genres[key];
        if (value) {
          allowGenres.push(genreMap[key]._id);
        } else {
          denyGenres.push(genreMap[key]._id);
        }
      });

      if (allowGenres.length && denyGenres.length) {
        options.categories = { '$all': allowGenres };
        options = {
          $and: [
            options,
            { categories: { '$not': { '$all': denyGenres } } }
          ]
        };
      } else if (allowGenres.length) {
        options.categories = { '$all': allowGenres };
      } else if (denyGenres.length) {
        options.categories = { '$not': { '$all': denyGenres } };
      }

      var query = MrManga.find(options).sort(sort);
      if (!isNaN(params.limit)) query.limit(parseInt(params.limit));
      query.exec(function (err, mangas) {
        if (err) {
          return done(100, err);
        } else {
          var arr = [];
          (mangas || []).forEach(function (m) {
            arr.push(m.oid);
          });
          done(null, arr);
        }
      });
    },
    function (mangas, done) {
      res.json({
        code: 0,
        data: mangas
      });
    }
  ], function (err, result) {
    res.json({
      'code': err,
      'data': result
    });
  });
};

exports.latestHomeManga = function (req, res) {
  return exports.latestManga(req, res, 50);
}

exports.latestManga = function (req, res, limit) {
  if (!limit) limit = 500;
  async.waterfall([
    function (done) {
      MrManga
        .find()
        .sort({ last_updated: -1 })
        .limit(1)
        .exec(function (err, mangas) {
          if (err || mangas.length == 0) {
            return done(100, 'Unknown error');
          }

          done(null, mangas[0].last_updated);
        });
    },
    function (last_updated, done) {
      var moment = new Date(last_updated);
      moment.setHours(moment.getHours() - 12);
      MrManga
        .find({ last_updated: { $gte: moment } })
        .limit(limit)
        .sort({ last_updated: -1 })
        .populate('chapters')
        .populate('categories')
        .populate('authors')
        .exec(function (err, mangas) {
          if (err) {
            return done(100, 'Unknown error');
          }

          var result = [];
          (mangas || []).forEach(function (m) {
            var item = {
              oid: m.oid,
              name: m.name,
              rank: m.rank,
              updated_chapters: 0,
              new_chapters: [],
              genres: [],
              completed: m.completed,
              thumbnail: m.thumbnail,
              updated_at: m.last_updated
            };
            (m.categories || []).forEach(function (c) {
              item.genres.push(c.oid);
            });
            m.chapters.forEach(function (c) {
              if (c.updatedAt >= moment) {
                item.new_chapters.push({
                  oid: c.oid,
                  name: c.name,
                  updatedAt: c.updatedAt
                });
              }
            });
            item.updated_chapters = item.new_chapters.length;
            result.push(item);
          });
          done(null, result);
        });
    },
    function (mangas, done) {
      res.json({
        code: 0,
        data: mangas
      });
    }
  ], function (err, result) {
    res.json({
      'code': err,
      'data': result
    });
  })
};

exports.trendMangas = function (req, res) {
  async.waterfall([
    function (done) {
      MrManga
        .find()
        .sort({ last_updated: -1 })
        .limit(1)
        .exec(function (err, mangas) {
          if (err || mangas.length == 0) {
            return done(100, 'Unknown error');
          }

          done(null, mangas[0].last_updated);
        });
    },
    function (last_updated, done) {
      var moment = new Date(last_updated);
      moment.setHours(moment.getHours() - 12);
      MrManga
        .find({ last_updated: { $gte: moment } })
        .limit(10)
        .sort({ rank: -1 })
        .sort({ last_updated: -1 })
        .populate('authors')
        .exec(function (err, mangas) {
          if (err) {
            return done(100, 'Unknown error');
          }

          var result = [];
          var i=1;
          (mangas || []).forEach(function (m) {
            var item = {
              mid: m.mid,
              oid: m.oid,
              name: m.name,
              author: (m.authors || []).length == 0 ? '' : m.authors[0].name,
              rank: i++,
              thumbnail: m.thumbnail
              // updated_at: m.last_updated
            };
            result.push(item);
          });
          done(null, result);
        });
    },
    function (mangas, done) {
      res.json({
        code: 0,
        data: mangas
      });
    }
  ], function (err, result) {
    res.json({
      'code': err,
      'data': result
    });
  })
};

exports.lastTrack = function (req, res) {
  MrTracking
    .findOne()
    .sort({ 'updated_at': -1 })
    .populate({
      path: 'sections',
      populate: {
        path: 'items.data'
      }
    })
    .exec(function (err, track) {
      if (err) {
        return res.json({
          'code': 100,
          'data': 'Unknow error'
        });
      } else {
        var result = {
          trackingId: track.trackingId,
          sections: []
        };
        for(var i = 0; i < track.sections.length; i++) {
          var s = track.sections[i];
          var obj = {
            sectionId: s.sectionId,
            title: s.title,
            subtitle: s.subtitle,
            type: s.type,
            items: {
              component: s.items.component,
              data: []
            }
          }
          s.items.data.forEach(function(c) {
            obj.items.data.push({
              oid: c.oid,
              title: { en: c.name },
              subtitle: { en: c.description },
              oids: c.items.map(function(m) {
                return m.oid
              }),
              banner_url: c.banner
            });
          });
          result.sections.push(obj);
        };
        
        res.json({
          'code': 0,
          'data': result
        })
      }
    });
};

exports.collection = function (req, res) {
  var oid = req.query.oid;
  MrCollection
    .findOne({ oid: oid })
    .exec(function (err, collect) {
      if (err) {
        return res.json({
          'code': 100,
          'data': 'Unknow error'
        });
      } else {
        return res.json({
          'code': 0,
          'data': collect
        })
      }
    })
};