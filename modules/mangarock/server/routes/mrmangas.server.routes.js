'use strict';

/**
 * Module dependencies
 */
var policy = require('../policies/mrmangas.server.policy'),
  admin_controller = require('../controllers/manga.admin.servicer.controller'),
  controller = require('../controllers/mrmangas.server.controller');

module.exports = function (app) {
  app.route('/api/mr/query/web400/genre')
    .get(controller.genre);

  app.route('/api/mr/query/web400/pages')
    .get(controller.pages);
  
  app.route('/api/mr/query/web400/info')
    .get(controller.manga);

  app.route('/api/mr/query/web400/multi_info')
    .post(controller.multiManga);

  app.route('/api/mr/query/web400/character')
    .get(controller.character);
  
    app.route('/api/mr/query/web400/mrs_serie_related_character')
    .get(controller.listMangaByCharacter);

  app.route('/api/mr/query/web400/author')
    .get(controller.author);

  app.route('/api/mr/query/web400/mrs_serie_related_author')
    .get(controller.listMangaByAuthor);

  app.route('/api/mr/query/web400/mrs_all_author')
    .post(controller.listAuthorByName);

  app.route('/api/mr/query/web400/rec_similar_series')
    .get(controller.similarMangas);
    
  app.route('/api/mr/meta')
    .post(controller.meta);

  app.route('/api/mr/query/web400/mrs_quick_search')
    .post(controller.quicksearch);

    app.route('/api/mr/query/web400/mrs_search')
    .post(controller.search);

  app.route('/api/mr/query/web400/mrs_filter')
    .post(controller.filter);

  app.route('/api/mr/query/web400/mrs_home_latest')
    .get(controller.latestHomeManga);

  app.route('/api/mr/query/web400/mrs_latest')
    .get(controller.latestManga);

  app.route('/api/mr/query/web400/realtime')
    .get(controller.trendMangas);

  app.route('/api/mr/query/web400/foryou')
    .get(controller.lastTrack)

  app.route('/api/mr/query/web400/collection')
    .get(controller.collection)

  // Finish by binding the Mgeden middleware
  // app.param('mgedenId', mgedens.mgedenByID);

  app.route('/api/adm/scrap/tasks')
    .get(admin_controller.pm2list)

  app.route('/api/adm/scrap/:script/:command')
    .get(admin_controller.pm2command)

  app.route('/api/adm/dash/info')
    .get(admin_controller.dashinfo)

};
