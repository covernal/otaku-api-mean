// Mgedens service used to communicate Mgedens REST endpoints
(function () {
  'use strict';

  angular
    .module('support')
    .factory('MgSupportsService', MgSupportsService);

  MgSupportsService.$inject = ['$resource'];

  function MgSupportsService($resource) {
    return $resource('api/mgedens/:mgedenId', {
      mgedenId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
}());
