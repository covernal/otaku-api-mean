(function () {
  'use strict';

  // Mgedens controller
  angular
    .module('support')
    .controller('MgSupportController', MgSupportController);

    MgSupportController.$inject = ['$scope', '$state', '$window', 'Authentication', 'mgedenResolve'];

  function MgSupportController($scope, $state, $window, Authentication, mgeden) {
    var vm = this;

    vm.authentication = Authentication;
    vm.mgeden = mgeden;
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;

    // Remove existing Mgeden
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.mgeden.$remove($state.go('mgedens.list'));
      }
    }

    // Save Mgeden
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.mgedenForm');
        return false;
      }

      // TODO: move create/update logic to service
      if (vm.mgeden._id) {
        vm.mgeden.$update(successCallback, errorCallback);
      } else {
        vm.mgeden.$save(successCallback, errorCallback);
      }

      function successCallback(res) {
        $state.go('mgedens.view', {
          mgedenId: res._id
        });
      }

      function errorCallback(res) {
        vm.error = res.data.message;
      }
    }
  }
}());
