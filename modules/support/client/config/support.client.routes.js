(function () {
  'use strict';

  angular
    .module('support')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      // .state('mgedens', {
      //   abstract: true,
      //   url: '/mgedens',
      //   template: '<ui-view/>'
      // })
      // .state('mgedens.list', {
      //   url: '',
      //   templateUrl: 'modules/mgedens/client/views/list-mgedens.client.view.html',
      //   controller: 'MgedensListController',
      //   controllerAs: 'vm',
      //   data: {
      //     pageTitle: 'Mgedens List'
      //   }
      // })
      // .state('mgedens.create', {
      //   url: '/create',
      //   templateUrl: 'modules/mgedens/client/views/form-mgeden.client.view.html',
      //   controller: 'MgedensController',
      //   controllerAs: 'vm',
      //   resolve: {
      //     mgedenResolve: newMgeden
      //   },
      //   data: {
      //     roles: ['user', 'admin'],
      //     pageTitle: 'Mgedens Create'
      //   }
      // })
      // .state('mgedens.edit', {
      //   url: '/:mgedenId/edit',
      //   templateUrl: 'modules/mgedens/client/views/form-mgeden.client.view.html',
      //   controller: 'MgedensController',
      //   controllerAs: 'vm',
      //   resolve: {
      //     mgedenResolve: getMgeden
      //   },
      //   data: {
      //     roles: ['user', 'admin'],
      //     pageTitle: 'Edit Mgeden {{ mgedenResolve.name }}'
      //   }
      // })
      // .state('mgedens.view', {
      //   url: '/:mgedenId',
      //   templateUrl: 'modules/mgedens/client/views/view-mgeden.client.view.html',
      //   controller: 'MgedensController',
      //   controllerAs: 'vm',
      //   resolve: {
      //     mgedenResolve: getMgeden
      //   },
      //   data: {
      //     pageTitle: 'Mgeden {{ mgedenResolve.name }}'
      //   }
      // });
  }

  getMgeden.$inject = ['$stateParams', 'MgedensService'];

  function getMgeden($stateParams, MgedensService) {
    return MgedensService.get({
      mgedenId: $stateParams.mgedenId
    }).$promise;
  }

  newMgeden.$inject = ['MgedensService'];

  function newMgeden(MgedensService) {
    return new MgedensService();
  }
}());
