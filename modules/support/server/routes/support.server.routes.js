'use strict';

/**
 * Module dependencies
 */
var //mgedensPolicy = require('../policies/mgedens.server.policy'),
  mgedens = require('../controllers/support.server.controller');

module.exports = function (app) {
  // // Scrap Mangas from MangaEden
  // app.route('/admin/api/mgedens/scrap/manga').all(mgedensPolicy.isAllowed)
  //   .get(mgedens.scrap);

  // // Mgedens Routes
  // app.route('/api/mgedens').all(mgedensPolicy.isAllowed)
  //   .get(mgedens.list);

  // app.route('/api/mgedens/:mgedenId').all(mgedensPolicy.isAllowed)
  //   .get(mgedens.read);

  // app.route('/api/mgedens/chapter/:chapterId').all(mgedensPolicy.isAllowed)
  //   .get(mgedens.pages);

  // // Finish by binding the Mgeden middleware
  // app.param('mgedenId', mgedens.mgedenByID);
};
