'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  async = require('async'),
  hash = require('object-hash'),
  axios = require('axios'),
  mongoose = require('mongoose'),
  MgSupport = mongoose.model('MgSupport'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

  exports.post = function (req, res) {
      // Init Variables
  var user = req.user;

  if (!user) {
    res.status(401).send({
      message: 'User is not signed in'
    });
  }

  if (!req.body.title || req.body.title === '') {
    res.status(400).send({
      message: 'Title can\'t be empty'
    });
  }

  var data = {};
  data.title = req.body.title;
  data.lowerTitle = data.title.toLowerCase();
  data.content = req.body.content;

}