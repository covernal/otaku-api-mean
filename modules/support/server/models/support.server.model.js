'use strict';

/**
 * Module dependencies.
 * Refer to https://www.mangaeden.com/api/
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * MgSupport Schema
 */
var MgSupportSchema = new Schema({
  // type
  // 0 - FAQ
  // 10 - What is New?
  type: {
    type: Number,
    default: 0 
  },  
  title: {
    type: String,
    default: '',
    required: 'Please fill Mgeden name',
    trim: true
  },
  // lowercase value for title
  lowerTitle: {
    type: String
  },
  content: {
    type: String,
    default: ''
  },
  lastUpdated: {
    type: Date,
    default: Date.now
  },  
  created: {
    type: Date,
    default: Date.now
  },
  author: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

// EXPOSE ONLY CERTAIN FIELDS
// It's really important that we keep
// stuff like password private!
MgSupportSchema.method('toJSON', function () {
  return {
    _id: this._id,
    type: this.type,
    title: this.title,
    content: this.content,
    lastUpdated: this.lastUpdated,
    created: this.created,
    author: this.author
  };
});

mongoose.model('MgSupport', MgSupportSchema);
