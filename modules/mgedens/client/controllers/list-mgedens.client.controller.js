(function () {
  'use strict';

  angular
    .module('mgedens')
    .controller('MgedensListController', MgedensListController);

  MgedensListController.$inject = ['MgedensService'];

  function MgedensListController(MgedensService) {
    var vm = this;

    vm.mgedens = MgedensService.query();
  }
}());
