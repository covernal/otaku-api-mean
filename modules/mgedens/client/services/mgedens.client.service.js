// Mgedens service used to communicate Mgedens REST endpoints
(function () {
  'use strict';

  angular
    .module('mgedens')
    .factory('MgedensService', MgedensService);

  MgedensService.$inject = ['$resource'];

  function MgedensService($resource) {
    return $resource('api/mgedens/:mgedenId', {
      mgedenId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
}());
