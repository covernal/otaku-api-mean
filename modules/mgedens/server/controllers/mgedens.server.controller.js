'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  async = require('async'),
  hash = require('object-hash'),
  axios = require('axios'),
  mongoose = require('mongoose'),
  Mgeden = mongoose.model('Mgeden'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');


/**
 * Scrap Mangas from https://www.mangaeden.com/api/
 */
exports.scrap = function (req, res) {
  console.log('start scarpping from eden.com');
  var page = req.query.page;
  var length = req.query.length;
  var url = 'https://www.mangaeden.com/api/list/0/?';
  url += page ? 'p=' + page : '';
  url += length ? '&l=' + length : '';
  axios.get(url)
    .then(function (response) {
      console.log('done scrapping with status', response.status);
      var mgMap = {};
      async.waterfall([
        function (done) {
          var data = response.data;
          if (data && data.manga && data.manga.length) {
            done(null, data);
          } else {
            done('Invalid data');
          }
        },
        function (data, done) {
          var bulk = Mgeden.collection.initializeUnorderedBulkOp();
          data.manga.forEach(mg => {
            var record = {
              identity: mg.i,
              title: mg.t,
              lowerTitle: mg.t.toLowerCase().replace(/[^a-z0-9 ]/g, ''),
              alias: mg.a,
              coverImage: mg.im,
              status: mg.s,
              category: mg.c,
              lastUpdated: new Date(mg.ld * 1000), // unix timestam to date
              hits: mg.h
            };
            record.hash = hash(record);
            mg.hash = record.hash;
            mgMap[mg.i] = mg;
            bulk.insert(record);
          });
          bulk.execute(function (err, result) {
            done(null, result);
          });
        },
        function (result, done) {
          if (!result.getWriteErrorCount()) {
            return done(null, result);
          }

          var bulk = Mgeden.collection.initializeUnorderedBulkOp();
          result.getWriteErrors().forEach(obj => {
            var objson = obj.toJSON();
            var _md = objson.op;
            var mg = mgMap[_md.identity];
            if (objson.code === 11000) { // only if duplicate key error
              var record = {
                identity: mg.i,
                title: mg.t,
                lowerTitle: mg.t.toLowerCase(),
                alias: mg.a,
                coverImage: mg.im,
                status: mg.s,
                category: mg.c,
                lastUpdated: new Date(mg.ld * 1000), // unix timestam to date
                hits: mg.h,
                hash: mg.hash
              };
              bulk.find({ identity: mg.i, hash: { $ne: mg.hash } }).upsert().updateOne(record);
            }
          });
          bulk.execute(function (err, result) {
            done(null, result);
          });
        },
        function (result, done) {
          res.json(result);
        }
      ], function (err) {
        if (err) {
          res.status(400).json(err);
        }
      });
    })
    .catch(function (err) {
      console.log('done scrapping with err', err);
      res.status(400).json('bad operation');
    });
};

// /**
//  * Create a Mgeden
//  */
// exports.create = function (req, res) {
//   var mgeden = new Mgeden(req.body);
//   mgeden.user = req.user;

//   mgeden.save(function (err) {
//     if (err) {
//       return res.status(400).send({
//         message: errorHandler.getErrorMessage(err)
//       });
//     } else {
//       res.jsonp(mgeden);
//     }
//   });
// };

/**
 * Show the current Mgeden
 */
exports.read = function (req, res) {
  // convert mongoose document to JSON
  var mgeden = req.mgeden ? req.mgeden.toJSON() : {};

  axios.get('https://www.mangaeden.com/api/manga/' + req.mgeden.identity)
    .then(function (response) {
      mgeden.body = response.data;
      var chapters = [];
      mgeden.body.chapters.forEach(ch => {
        chapters.push({
          number: ch[0],
          _id: ch[3],
          created: new Date(ch[1] * 1000),
          title: ch[2] || ''
        });
      });
      mgeden.body.chapters = chapters;
      mgeden.body.created = new Date(mgeden.body.created * 1000);
      mgeden.body.last_chapter_date = new Date(mgeden.body.last_chapter_date * 1000);
      mgeden.body.image = 'https://cdn.mangaeden.com/mangasimg/' + mgeden.body.image;
      res.json(mgeden);
    })
    .catch(function (err) {
      console.log('done scrapping with err', err);
      res.status(400).json('bad operation');
    });
};

exports.pages = function (req, res) {
  axios.get('https://www.mangaeden.com/api/chapter/' + req.params.chapterId)
    .then(function (response) {
      var data = response.data;
      var pages = [];
      data.images.forEach(img => {
        pages.push({
          number: img[0],
          url: 'https://cdn.mangaeden.com/mangasimg/' + img[1],
          width: img[2],
          height: img[3]
        });
      });
      res.json(pages);
    })
    .catch(function (err) {
      console.log('done scrapping with err', err);
      res.status(400).json('bad operation');
    });
};

// /**
//  * Update a Mgeden
//  */
// exports.update = function (req, res) {
//   var mgeden = req.mgeden;

//   mgeden = _.extend(mgeden, req.body);

//   mgeden.save(function (err) {
//     if (err) {
//       return res.status(400).send({
//         message: errorHandler.getErrorMessage(err)
//       });
//     } else {
//       res.jsonp(mgeden);
//     }
//   });
// };

// /**
//  * Delete an Mgeden
//  */
// exports.delete = function (req, res) {
//   var mgeden = req.mgeden;

//   mgeden.remove(function (err) {
//     if (err) {
//       return res.status(400).send({
//         message: errorHandler.getErrorMessage(err)
//       });
//     } else {
//       res.jsonp(mgeden);
//     }
//   });
// };

/**
 * List of Mgedens
 */
exports.list = function (req, res) {
  var page = req.query.page * 1 || 0;
  var length = req.query.length * 1 || 25;
  var order = req.query.order || 'newest';
  var title = req.query.title;
  var opCategry = req.query.opCategory || 'all';
  var categries = req.query.category || '';
  categries = categries.trim();
  categries = categries === '' ? [] : categries.split(' ');

  var query = Mgeden.find();
  if (order === 'newest') query.sort({ 'lastUpdated': -1 });
  if (order === 'oldest') query.sort({ 'lastUpdated': -1 });
  if (order === 'a-z') query.sort({ 'lowerTitle': +1 });
  if (order === 'z-a') query.sort({ 'lowerTitle': -1 });
  if (order === 'hits') query.sort({ 'hits': -1 });

  if (categries.length && opCategry === 'all') query.where('category', { $all: categries });
  if (categries.length && opCategry === 'in') query.where('category', { $in: categries });
  if (title) query.where('lowerTitle').regex(new RegExp(title, 'g'));
  
  query.skip(page * length)
    .limit(length)
    .exec(function (err, mgedens) {
      if (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      } else {
        res.jsonp(mgedens);
      }
    });
};

/**
 * Mgeden middleware
 */
exports.mgedenByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Mgeden is invalid'
    });
  }

  Mgeden.findById(id).populate('user', 'displayName').exec(function (err, mgeden) {
    if (err) {
      return next(err);
    } else if (!mgeden) {
      return res.status(404).send({
        message: 'No Mgeden with that identifier has been found'
      });
    }
    req.mgeden = mgeden;
    next();
  });
};
