'use strict';

/**
 * Module dependencies.
 * Refer to https://www.mangaeden.com/api/
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Mgeden Schema
 */
var MgedenSchema = new Schema({
  // identity number from MangaEden - i
  identity: {
    type: String,
    unique: true,
    required: 'Please fill identity number from MangaEden'
  },
  // ISO 639-2 code style
  language: {
    type: String,
    default: 'eng'
  },
  // title - t
  title: {
    type: String,
    default: '',
    required: 'Please fill Mgeden name',
    trim: true
  },
  // lowercase value for title
  lowerTitle: {
    type: String
  },
  // alias - a
  alias: {
    type: String,
    default: '',
    trim: true
  },
  // cover image - im
  coverImage: {
    type: String,
    default: ''
  },
  // manga status from eden. - s
  // 0 - suspended, 1 - onging, 2 - completed
  status: {
    type: String,
    default: ''
  },
  // categories - string array - c
  category: [String],
  // last chapter date - ld
  lastUpdated: {
    type: Date,
    default: Date.now
  },
  // hits - h
  hits: {
    type: Number,
    default: 0
  },
  // inserted date
  created: {
    type: Date,
    default: Date.now
  },
  hash: {
    type: String
  }
  // // user who inserted
  // user: {
  //   type: Schema.ObjectId,
  //   ref: 'User'
  // }
});

// EXPOSE ONLY CERTAIN FIELDS
// It's really important that we keep
// stuff like password private!
MgedenSchema.method('toJSON', function () {
  return {
    _id: this._id,
    language: this.language,
    title: this.title,
    alias: this.alias,
    coverImage: 'https://cdn.mangaeden.com/mangasimg/' + this.coverImage,
    status: this.status,
    category: this.category,
    lastUpdated: this.lastUpdated,
    hits: this.hits
  };
});

mongoose.model('Mgeden', MgedenSchema);
