'use strict';

/**
 * Module dependencies
 */
var acl = require('acl');

// Using the memory backend
acl = new acl(new acl.memoryBackend());

/**
 * Invoke Mgedens Permissions
 */
exports.invokeRolesPolicies = function () {
  acl.allow([{
    roles: ['guest'],
    allows: [{
      resources: '/admin/api/mgedens/scrap/manga',
      permissions: '*'
    }, {
      resources: '/admin/api/mgedens/scrap/chapter',
      permissions: '*'
    }, {
      resources: '/api/mgedens',
      permissions: '*'
    }, {
      resources: '/api/mgedens/:mgedenId',
      permissions: '*'
    }, {
      resources: '/api/mgedens/chapter/:chapterId',
      permissions: '*'
    }]
  }]);
};

/**
 * Check If Mgedens Policy Allows
 */
exports.isAllowed = function (req, res, next) {
  var roles = (req.user) ? req.user.roles : ['guest'];

  // If an Mgeden is being processed and the current user created it then allow any manipulation
  if (req.mgeden && req.user && req.mgeden.user && req.mgeden.user.id === req.user.id) {
    return next();
  }

  // Check for user roles
  acl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), function (err, isAllowed) {
    if (err) {
      // An authorization error occurred
      return res.status(500).send('Unexpected authorization error');
    } else {
      if (isAllowed) {
        // Access granted! Invoke next middleware
        return next();
      } else {
        return res.status(403).json({
          message: 'User is not authorized'
        });
      }
    }
  });
};
