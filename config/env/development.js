'use strict';

var defaultEnvConfig = require('./default');

module.exports = {
  db: {
    uri: process.env.MONGOHQ_URL || process.env.MONGODB_URI || 'mongodb://' + (process.env.DB_1_PORT_27017_TCP_ADDR || 'localhost') + '/otaku',
    options: {},
    // Enable mongoose debug mode
    debug: process.env.MONGODB_DEBUG || false
  },
  log: {
    // logging with Morgan - https://github.com/expressjs/morgan
    // Can specify one of 'combined', 'common', 'dev', 'short', 'tiny'
    format: 'dev',
    fileLogger: {
      directoryPath: process.cwd(),
      fileName: 'app.log',
      maxsize: 10485760,
      maxFiles: 2,
      json: false
    }
  },
  app: {
    title: defaultEnvConfig.app.title + ' - Development Environment'
  },
  facebook: {
    clientID: process.env.FACEBOOK_ID || 'APP_ID',
    clientSecret: process.env.FACEBOOK_SECRET || 'APP_SECRET',
    callbackURL: '/api/auth/facebook/callback'
  },
  twitter: {
    username: '@TWITTER_USERNAME',
    clientID: process.env.TWITTER_KEY || 'CONSUMER_KEY',
    clientSecret: process.env.TWITTER_SECRET || 'CONSUMER_SECRET',
    callbackURL: '/api/auth/twitter/callback'
  },
  google: {
    clientID: process.env.GOOGLE_ID || 'APP_ID',
    clientSecret: process.env.GOOGLE_SECRET || 'APP_SECRET',
    callbackURL: '/api/auth/google/callback'
  },
  linkedin: {
    clientID: process.env.LINKEDIN_ID || 'APP_ID',
    clientSecret: process.env.LINKEDIN_SECRET || 'APP_SECRET',
    callbackURL: '/api/auth/linkedin/callback'
  },
  github: {
    clientID: process.env.GITHUB_ID || 'APP_ID',
    clientSecret: process.env.GITHUB_SECRET || 'APP_SECRET',
    callbackURL: '/api/auth/github/callback'
  },
  paypal: {
    clientID: process.env.PAYPAL_ID || 'CLIENT_ID',
    clientSecret: process.env.PAYPAL_SECRET || 'CLIENT_SECRET',
    callbackURL: '/api/auth/paypal/callback',
    sandbox: true
  },
  mailer: {
    from: process.env.MAILER_FROM || 'support@otakulife.app',
    options: {
      auth: {
        api_key: '0b79de84923f7e2c8f1c01efde67b7f6-f45b080f-bbf3c552',
        // api_key: 'pubkey-f5b574368ce71f070e77d645735fb141',
        domain: 'mg.otakulife.app'
      }
      // service: process.env.MAILER_SERVICE_PROVIDER || 'MAILER_SERVICE_PROVIDER',
      // auth: {
      //   user: process.env.MAILER_EMAIL_ID || 'MAILER_EMAIL_ID',
      //   pass: process.env.MAILER_PASSWORD || 'MAILER_PASSWORD'
      // }
    }
  },
  livereload: true,
  seedDB: {
    seed: process.env.MONGO_SEED === 'true',
    options: {
      logResults: process.env.MONGO_SEED_LOG_RESULTS !== 'false'
    },
    // Order of collections in configuration will determine order of seeding.
    // i.e. given these settings, the User seeds will be complete before
    // Article seed is performed.
    collections: [{
      model: 'User',
      docs: [{
        data: {
          username: 'otakumaster',
          email: 'otakumaster@localhost.com',
          firstName: 'Otaku',
          lastName: 'Master',
          roles: ['admin', 'user']
        }
      }]
    }, {
      model: 'MrGenre',
      docs: [
        { data: { 'oid': 'mrs-genre-100117675', 'name': '4-koma' } },
        { data: { 'oid': 'mrs-genre-304068', 'name': 'Action' } },
        { data: { 'oid': 'mrs-genre-358370', 'name': 'Adult' } },
        { data: { 'oid': 'mrs-genre-304087', 'name': 'Adventure' } },
        { data: { 'oid': 'mrs-genre-304069', 'name': 'Comedy' } },
        { data: { 'oid': 'mrs-genre-304088', 'name': 'Demons' } },
        { data: { 'oid': 'mrs-genre-304197', 'name': 'Doujinshi' } },
        { data: { 'oid': 'mrs-genre-304177', 'name': 'Drama' } },
        { data: { 'oid': 'mrs-genre-304074', 'name': 'Ecchi' } },
        { data: { 'oid': 'mrs-genre-304089', 'name': 'Fantasy' } },
        { data: { 'oid': 'mrs-genre-304358', 'name': 'Gender Bender' } },
        { data: { 'oid': 'mrs-genre-304075', 'name': 'Harem' } },
        { data: { 'oid': 'mrs-genre-304306', 'name': 'Historical' } },
        { data: { 'oid': 'mrs-genre-304259', 'name': 'Horror' } },
        { data: { 'oid': 'mrs-genre-304070', 'name': 'Josei' } },
        { data: { 'oid': 'mrs-genre-304846', 'name': 'Kids' } },
        { data: { 'oid': 'mrs-genre-304090', 'name': 'Magic' } },
        { data: { 'oid': 'mrs-genre-304072', 'name': 'Martial Arts' } },
        { data: { 'oid': 'mrs-genre-358371', 'name': 'Mature' } },
        { data: { 'oid': 'mrs-genre-304245', 'name': 'Mecha' } },
        { data: { 'oid': 'mrs-genre-304091', 'name': 'Military' } },
        { data: { 'oid': 'mrs-genre-304589', 'name': 'Music' } },
        { data: { 'oid': 'mrs-genre-304178', 'name': 'Mystery' } },
        { data: { 'oid': 'mrs-genre-100018505', 'name': 'One Shot' } },
        { data: { 'oid': 'mrs-genre-304786', 'name': 'Parody' } },
        { data: { 'oid': 'mrs-genre-304236', 'name': 'Police' } },
        { data: { 'oid': 'mrs-genre-304176', 'name': 'Psychological' } },
        { data: { 'oid': 'mrs-genre-304073', 'name': 'Romance' } },
        { data: { 'oid': 'mrs-genre-304076', 'name': 'School Life' } },
        { data: { 'oid': 'mrs-genre-304180', 'name': 'Sci-Fi' } },
        { data: { 'oid': 'mrs-genre-304077', 'name': 'Seinen' } },
        { data: { 'oid': 'mrs-genre-304175', 'name': 'Shoujo' } },
        { data: { 'oid': 'mrs-genre-304695', 'name': 'Shoujo Ai' } },
        { data: { 'oid': 'mrs-genre-304164', 'name': 'Shounen' } },
        { data: { 'oid': 'mrs-genre-304307', 'name': 'Shounen Ai' } },
        { data: { 'oid': 'mrs-genre-304195', 'name': 'Slice of Life' } },
        { data: { 'oid': 'mrs-genre-358372', 'name': 'Smut' } },
        { data: { 'oid': 'mrs-genre-305814', 'name': 'Space' } },
        { data: { 'oid': 'mrs-genre-304367', 'name': 'Sports' } },
        { data: { 'oid': 'mrs-genre-305270', 'name': 'Super Power' } },
        { data: { 'oid': 'mrs-genre-304067', 'name': 'Supernatural' } },
        { data: { 'oid': 'mrs-genre-358379', 'name': 'Tragedy' } },
        { data: { 'oid': 'mrs-genre-304765', 'name': 'Vampire' } },
        { data: { 'oid': 'mrs-genre-358150', 'name': 'Webtoons' } },
        { data: { 'oid': 'mrs-genre-304202', 'name': 'Yaoi' } },
        { data: { 'oid': 'mrs-genre-304690', 'name': 'Yuri' } }
      ]
    }]
  }
};
